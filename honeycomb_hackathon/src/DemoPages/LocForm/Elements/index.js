import React, {Fragment} from 'react'

import PageTitle from '../../../Layout/AppMain/PageTitle';

// Examples

import FormsDefault from './Controls/CreateLoC';

class FormElementsControls extends React.Component {

    render() {
        return (
            <Fragment>
                <PageTitle
                    heading="Form Controls"
                    subheading="Wide selection of forms controls, using the Bootstrap 4 code base, but built with React."
                    icon="pe-7s-display1 icon-gradient bg-premium-dark"
                />
                <FormsDefault />
            </Fragment>
        )
    }
}

export default FormElementsControls;



