import React, { Component } from 'react';
import CanvasJSReact from "../../../../../assets/canvasjs.react";
import {Label} from 'reactstrap';


var CanvasJSChart = CanvasJSReact.CanvasJSChart;


 
class PieChart extends Component {
	render() {
		const options = {
			exportEnabled: true,
			animationEnabled: true,
			title: {
				text: "Scheme Details"
			},
			data: [{
				type: "pie",
				startAngle: 75,
				toolTipContent: "<b>{label}</b>: {y}%",
				showInLegend: "true",
				legendText: "{label}",
				indexLabelFontSize: 16,
				indexLabel: "{label} - {y}%",
				dataPoints: [
					{ y: 20, label: "Super Saver" },
					{ y: 49, label: "Extra Protection" },
					{ y: 31, label: "Farmer Friendly" },
				]
			}]
		}
		
		return (
		<div>
			<h1>Insurance Policy Details</h1>
			<CanvasJSChart options = {options} 
				/* onRef={ref => this.chart = ref} */
			/>
			<div style={{marginTop:"10px"}}>
				<Label>Total Insurance Policies : {this.props.data.totalSchemeCount}</Label><br/>
				<Label>Registered Farmers : {this.props.data.totalRegFarmersCount} </Label><br/>
				<Label>Total Indemnity Paid : {this.props.data.totalPaid} LINK</Label><br/>
			</div>
		</div>
		);
	}
}

export default PieChart;