import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';

import TxStatus from "../../commonComponents/StatusCrad";


export default class CompanyRegistrationForm extends React.Component {
    constructor(props) {
        super(props);
        console.log("In insurance company registration form..., contractInstance : ", this.props.contractInstance);
        this.state = {
            companyName : "",
            des : "",
            address : "",
            depositedAmount : "",
            isShowingStatus : false,
            txHash : "",
        }
        this.handleRegisterBtn = this.handleRegisterBtn.bind(this);
        this.updateName = this.updateName.bind(this);
        this.updateDes = this.updateDes.bind(this);
        this.updateAddress = this.updateAddress.bind(this);

        this.props.contractInstance.getBalanceInLink(function(err, balance) {
            if(!err) {
                this.setState({
                    depositedAmount : balance,
                });
            }
        }.bind(this));
    }

    componentDidMount() {
        
    }

    updateName(event) {
        this.setState({
            companyName : event.target.value,
        });
    }

    updateDes(event) {
        this.setState({
            des : event.target.value,
        });
    }

    updateAddress(event) {
        this.setState({
            address : event.target.value,
        });
    }

    handleRegisterBtn(event) {
        console.log("Register a compnay with these values : ", this.state.companyName, this.state.des, this.state.address, this.state.depositedAmount);
        event.preventDefault();
        
        this.props.contractInstance.registerCompany (
            this.state.companyName, this.state.des, this.state.address, this.state.depositedAmount, function(err, txHash) {
                if(!err && txHash) {
                    console.log("Transaction hash : ", txHash);
                        this.setState({
                            isShowingStatus : true,
                            txHash : txHash,
                        });
                } else {
                    console.log("Error : ", err);
                }
            }.bind(this));
    }



    render() {
        return (
            <div style={{textAlign:"center", padding:"30px", marginLeft:"200px", marginRight:"200px"}}>
                {this.state.isShowingStatus ? <h4><strong>Transaction Status</strong></h4> : <h4><strong>Compnay Registration</strong></h4>
                }
                
                <br/><br/>
                {
                    this.state.isShowingStatus ? 
                        <TxStatus contractInstance={this.props.contractInstance} txHash = {this.state.txHash}/>
                        :
                        <Row className="text-center">
                            <Col md="3"></Col>
                            <Col md="6">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <Form style={{fontSize:"12px"}}>
                                            <FormGroup>
                                                <Label>Your Blockchain Address : {this.props.contractInstance.loggedInAccount}</Label>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Enter Company Name : </Label>
                                                <Input style={{fontSize:"12px"}} type="text" placeholder="Framer Friendly Insurance Company Ltd"
                                                    onChange = {this.updateName} />
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Enter Company Description : </Label>
                                                <Input style={{fontSize:"12px"}} type="text" placeholder="Description"
                                                    onChange = {this.updateDes} />
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Enter Address : </Label>
                                                <Input style={{fontSize:"12px"}} type="text" placeholder="12/3, Park street, London" 
                                                    onChange = {this.updateAddress} />
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Your Deposited Toke (in LINK) : {this.state.depositedAmount} LINK</Label>
                                            </FormGroup>
                                            <button onClick={this.handleRegisterBtn} type="button" className="btn btn-primary" style={{width:"100%"}}>Submit</button>
                                        </Form>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="3"></Col>
                        </Row>
                }
            </div>   
                                                     
        )
    }
}