import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';




export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        console.log("In insurance company registration form...");
        this.state = {
            name : "",
            description : "",
            address : "",
            totalCapital : "",
            totalSchemeCount : "",
            totalRegFarmersCount : "",
        }

    }

    componentDidMount() {
        this.props.contractInstance.getCompanyInfo(this.props.companyId, function(err, result) {
            if(!err) {
                console.log("Company details : ", result);
                
                this.setState({
                    name : result[0],
                    description : result[1],
                    address : result[2],
                    totalCapital : Number(result[3]) / Math.pow(10, 18),
                    totalSchemeCount : Number(result[4]),
                    totalRegFarmersCount : Number(result[5]),
                });

                console.log("Companies in state : ", this.state);
            }
        }.bind(this));
    }



    render() {
        return (
            <div style={{textAlign:"center", paddingTop:"30px", marginTop:"30px", marginLeft:"200px", marginRight:"200px"}}>
                <h4><strong>{this.state.name}</strong></h4>
                <Row className="text-center">
                    <Col md="3"></Col>
                    <Col md="6">
                        <Card className="main-card mb-3 widget-chart">
                            <CardBody>
                                

                                <Form style={{fontSize:"12px"}}>
                                    <FormGroup>
                                        <Label>Your Blockchain Address : {this.props.contractInstance.loggedInAccount}</Label>
                                    </FormGroup>
                                   
                                    <FormGroup>
                                        <Label style={{color:"#6c757d"}}>{this.state.description}</Label>
                                    </FormGroup>

                                    <FormGroup>
                                        <Label>Address : {this.state.address}</Label>
                                    </FormGroup>

                                    <FormGroup>
                                        <Label>Total Capital (in LINK) : {this.state.totalCapital} LINK</Label>
                                    </FormGroup>

                                    <FormGroup>
                                        <Label>Total Insurance Policies : {this.state.totalSchemeCount}</Label>
                                    </FormGroup>

                                    <FormGroup>
                                        <Label>Total Registered Farmer : {this.state.totalRegFarmersCount}</Label>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col md="3"></Col>
                </Row>
            </div>   
                                                     
        )
    }
}