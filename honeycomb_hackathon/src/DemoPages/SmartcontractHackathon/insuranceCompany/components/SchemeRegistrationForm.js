import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';

import TxStatus from "../../commonComponents/StatusCrad";


const monthMapping = [
     {
        "startDate" : "2019-01-01",
        "endDate" : "2019-01-25",
    },
    {
        "startDate" : "2019-02-01",
        "endDate" : "2019-02-25",
    },
    {
        "startDate" : "2019-03-01",
        "endDate" : "2019-03-25",
    },
    {
        "startDate" : "2019-04-01",
        "endDate" : "2019-04-25",
    },
    {
        "startDate" : "2019-05-01",
        "endDate" : "2019-05-25",
    },
    {
        "startDate" : "2019-06-01",
        "endDate" : "2019-06-25",
    },
    {
        "startDate" : "2019-07-01",
        "endDate" : "2019-07-25",
    },
    {
        "startDate" : "2019-08-01",
        "endDate" : "2019-08-25",
    },
    {
        "startDate" : "2019-09-01",
        "endDate" : "2019-09-25",
    },
    {
        "startDate" : "2019-10-01",
        "endDate" : "2019-10-25",
    },
    {
        "startDate" : "2019-11-01",
        "endDate" : "2019-11-25",
    },
    {
        "startDate" : "2019-12-01",
        "endDate" : "2019-12-25",
    },

];

const stateMapping = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut","Delaware","Florida","Georgia","Hawaii"];

export default class SchemeRegistrationForm extends React.Component {
    constructor(props) {
        super(props);
        console.log("In scheme registration form...");
        this.state = {
            schemeName:"",
            cropsName : "",
            location : "",
            schemePrice : "",
            selectedMonth : "",
            address : "",
            isShowingStatus : false,
            txHash : "",
        }
        this.handleRegisterBtn = this.handleRegisterBtn.bind(this);
        this.updateSchemeName = this.updateSchemeName.bind(this);
        this.updateCropsName = this.updateCropsName.bind(this);
        this.selectLocation = this.selectLocation.bind(this);
        this.updateSchemePrice = this.updateSchemePrice.bind(this);
        this.selectMonth = this.selectMonth.bind(this);

        console.log("Month mapping : ", monthMapping[0].startDate, monthMapping[0].endDate);

    }

    componentDidMount() {

    }

    updateSchemeName(event) {
        this.setState({
            schemeName : event.target.value,
        });
    }

    updateCropsName(event) {
        this.setState({
            cropsName : event.target.value,
        });
    }

    selectLocation(event) {
        console.log("Location : ", event.target.value);
        this.setState({
            location : event.target.value,
        });
    }

    updateSchemePrice(event) {
        this.setState({
            schemePrice : event.target.value,
        });
    }

    selectMonth(event) {
        console.log("Month value : ", event.target.value);
        this.setState({
            selectedMonth : event.target.value,
        });
    }

    handleRegisterBtn(event) {
        console.log("Register a scheme with these values : ", this.state.schemeName, this.state.cropsName, 
                        stateMapping[this.state.location], this.state.schemePrice, 
                        monthMapping[this.state.selectedMonth].startDate, monthMapping[this.state.selectedMonth].endDate);

        event.preventDefault();

        this.props.contractInstance.addScheme (
            this.props.companyId, this.state.schemeName, this.state.cropsName, stateMapping[this.state.location], 
            this.state.schemePrice, monthMapping[this.state.selectedMonth].startDate, 
            monthMapping[this.state.selectedMonth].endDate, function(err, txHash) {
                if(!err && txHash) {
                    console.log("Transaction hash : ", txHash);
                        this.setState({
                            isShowingStatus : true,
                            txHash : txHash,
                        });
                } else {
                    console.log("Error : ", err);
                }
            }.bind(this));

    }



    render() {
        return (
            <div style={{textAlign:"center", padding:"30px", marginLeft:"200px", marginRight:"200px"}}>

                { this.state.isShowingStatus ? 
                    <h4><strong>Transaction Status</strong></h4> 
                    : 
                    <h4><strong>Insurance Policy Registration</strong></h4>
                }
                
                <br/><br/>
                {
                    this.state.isShowingStatus ? 
                        <TxStatus contractInstance={this.props.contractInstance} txHash = {this.state.txHash}/>
                        :
                        <Row className="text-center">
                            <Col md="3"></Col>
                            <Col md="6">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <Form style={{fontSize:"12px"}}>
                                            <FormGroup>
                                                <Label>Your Blockchain Address : {this.props.contractInstance.loggedInAccount}</Label>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Enter Scheme Name : </Label>
                                                <Input style={{fontSize:"12px"}} type="text" placeholder="Enter you insurance policy name"
                                                    onChange = {this.updateSchemeName} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label>Enter Crops Name : </Label>
                                                <Input style={{fontSize:"12px"}} type="text" placeholder="Enter crops name"
                                                    onChange = {this.updateCropsName} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label>Enter Scheme Price (In LINK) : </Label>
                                                <Input style={{fontSize:"12px"}} type="text" placeholder="Enter policy price"
                                                    onChange = {this.updateSchemePrice} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label>Select Location : </Label>
                                                <select onClick={this.selectLocation} style={{ fontSize:"12px", width:"280px", height:"35px", backgroundColor:"ligthGray"}}>
                                                    <option value="" disabled selected>Select Area</option>
                                                    <option value="0">Alabama</option>
                                                    <option value="1">Alaska</option>
                                                    <option value="2">Arizona</option>
                                                    <option value="3">Arkansas</option>
                                                    <option value="4">California</option>
                                                    <option value="5">Colorado</option>
                                                    <option value="6">Connecticut</option>
                                                    <option value="7">Delaware</option>
                                                    <option value="8">Florida</option>
                                                    <option value="9">Georgia</option>
                                                    <option value="10">Hawaii</option>
                                                </select>
                                            </FormGroup>
                                        
                                            <FormGroup>
                                                <Label>Select Active Month : </Label>
                                                <select onClick={this.selectMonth} style={{ fontSize:"12px", width:"280px", height:"35px", backgroundColor:"ligthGray"}}>
                                                    <option value="" disabled selected>Select Month</option>
                                                    <option value="0">Jan</option>
                                                    <option value="1">Feb</option>
                                                    <option value="2">March</option>
                                                    <option value="3">April</option>
                                                    <option value="4">May</option>
                                                    <option value="5">June</option>
                                                    <option value="6">July</option>
                                                    <option value="7">August</option>
                                                    <option value="8">Sep</option>
                                                    <option value="9">Oct</option>
                                                    <option value="10">Nov</option>
                                                    <option value="11">Dec</option>

                                                </select>
                                            </FormGroup>
                                            
                                            <button onClick={this.handleRegisterBtn} type="button" className="btn btn-primary" style={{width:"100%"}}>Add Scheme</button>
                                        </Form>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="3"></Col>
                        </Row>
                    }
            </div>   
                                                     
        );
    }
}