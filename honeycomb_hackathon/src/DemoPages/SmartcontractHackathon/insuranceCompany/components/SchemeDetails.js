import React from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';


import PieChart from "./schemeDetetailsComponents/PieChart";
import SchemeDetailsCard from "./schemeDetetailsComponents/SchemeDetailsCard";


const CROPS_INSURANCE_CONTRACT_ADDR = "0x28d93e988f703103e0b8c2c721d001055ba282ce";
const MonthMapping = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
export default class SchemeDetails extends React.Component {
    constructor(props) {
        super(props);
        console.log("In scheme details page...");
        this.state = {
            companyId : null,
            schemeDetails : [],
            pieChartData : {
                "totalSchemeCount" : 0,
                "totalRegFarmersCount" : 0,
                "totalPaid" : 0,
            },
        }
        this.getSchemeDetails = this.getSchemeDetails.bind(this);
        this.getPieChartData = this.getPieChartData.bind(this);
        this.showSchemeCards = this.showSchemeCards.bind(this);
    }

    componentDidMount() {
        let loggedInAddr = "0x" +  "000000000000000000000000" + this.props.contractInstance.loggedInAccount.slice(2);
        let options = {
            fromBlock: 0,
            toBlock: 'latest',
            topics:["0x08a8cea81d39388ee27cc2a7f50558d39a413b44245d345a8264b7353ffa3167",loggedInAddr, null], 
        };

        let filter =  this.props.contractInstance.web3.eth.filter(options);
        filter.get(function(error, result) {
            for(let i = result.length - 1; i >= 0; i--) {
                if(!error && result[i].address == CROPS_INSURANCE_CONTRACT_ADDR) {
                    console.log("Result : ", result);
                    this.setState({
                        companyId : parseInt(result[result.length - 1].topics[2]),
                    });
                    this.getSchemeDetails();
                    this.getPieChartData();
                    break;
                }
            }
            
        }.bind(this));
    }


    getSchemeDetails() {
        console.log("Company id : ", this.state.companyId);
        this.props.contractInstance.getSchemeCount(this.state.companyId, function(err, totalScheme) {
            if(!err) {
                let total = Number(totalScheme);
                console.log("Total scheme : ", total);
                for(let i = 0; i < total; i++) {
                    this.props.contractInstance.getSchemeInfo(this.state.companyId, i, function(err, result) {
                        if(!err) {
                            console.log("Scheme details : ", result);

                            let schemeDetailList = this.state.schemeDetails;
                            let schemeId = Number(result[0]);
                            let monthValue = Number(result[5].split("-")[1]);
                            console.log("Month value : ", monthValue);

                            let obj = {
                                companyId : this.state.companyId,
                                schemeId : schemeId,
                                schemeName : result[1],
                                cropsName : result[2],
                                location : result[3],
                                schemePrice : Number(result[4]),
                                month : MonthMapping[monthValue - 1],
                            }

                            schemeDetailList.push(obj);
                            
                            this.setState({
                                schemeDetails : schemeDetailList,
                            });

                            console.log("Scheme in state : ", this.state.schemeDetails);
                        }
                    }.bind(this));
                }
            }
        }.bind(this));
    }


    getPieChartData() {
        console.log("Company id : ", this.state.companyId);
        this.props.contractInstance.getPieChartData(this.state.companyId, function(err, result) {
            if(!err) {
                console.log("Pie chart data : ", result);
                let pieChartData = this.state.pieChartData;
                pieChartData.totalSchemeCount = Number(result[0]);
                pieChartData.totalRegFarmersCount = Number(result[1]);
                pieChartData.totalPaid = Number(result[2]);

                this.setState({
                    pieChartData : pieChartData,
                });

                console.log("Pie chart data in state : ", this.state.pieChartData);
            }
        }.bind(this));
    }

    showSchemeCards() {
        const schemeList = this.state.schemeDetails.map((scheme, index) => {
            return (
                <Col md="4" key={index}>
                    <SchemeDetailsCard contractInstance = {this.props.contractInstance}  scheme = {scheme}/>
                </Col>
            );
        });
        return schemeList;
    }


    render() {
        const schemeList = this.showSchemeCards();
        return (
            <div style={{textAlign:"center", padding:"30px"}}>
                <h3>Insurance Policies</h3>
                <br/><br/>
                <Row className="text-center">
                    
                    {schemeList}
                </Row>

                <br/>
                <Row className="text-center">
                    <Col md="2"></Col>
                    <Col md="8">
                        <Card className="main-card mb-3 widget-chart">
                            <CardBody>
                                {
                                    this.state.pieChartData.totalSchemeCount == 0 ?
                                        <h3>No Insurance Policy is registered.</h3>
                                        :
                                        <PieChart data = {this.state.pieChartData}/>
                                }
                                
                            </CardBody>
                        </Card>
                    </Col>
                    <Col md="2"></Col>
                </Row>
            </div>   
                                                     
        )
    }
}