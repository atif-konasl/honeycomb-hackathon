import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle
} from 'reactstrap';

import CompanyRegistrationForm from "./components/CompanyRegistrationForm"; 
import SchemeRegistrationFrom from "./components/SchemeRegistrationForm";
import SchemeDetails from "./components/SchemeDetails";
import Contract from "../blockchain/InsuranceContract";
import Profile from "./components/Profile";
import Metamask from "../commonComponents/MetaMaskInstallStatus";

//Background Image
import bg1 from "../../../assets/insurance/homepage_bg.jpeg";



const CROPS_INSURANCE_CONTRACT_ADDR = "0x28d93e988f703103e0b8c2c721d001055ba282ce";
export default class InsuranceCompnayPage extends React.Component {
    constructor(props) {
        super(props);
        console.log("In insurance company page...");
        this.state = {
            selectedOption : "",
            accountAddr : "",
            contractInstance : null,
            showProfile : false,
            companyId : null,
            metamaskConStatus : true,
        }
        this.handleSelectionBtn = this.handleSelectionBtn.bind(this);
        this.getCompanyId = this.getCompanyId.bind(this);

        this.contractInstance = new Contract();
        this.contractInstance.initMetamask(function(err, result) {
            if(!err) { 
                console.log("Metamask is connected successfully. Account address : ", this.contractInstance.loggedInAccount);
                this.setState({
                    accountAddr : this.contractInstance.loggedInAccount,
                    contractInstance : this.contractInstance,
                    metamaskConStatus : false,
                });
                this.getCompanyId()
            } else {
                this.setState({
                    metamaskConStatus :true,
                });
                console.log("Metamask status : ", this.state.metamaskConStatus);
                console.log(err);
            }
        }.bind(this));
    }

    componentDidMount() {
        
    }

    getCompanyId() {
        let loggedInAddr = "0x" +  "000000000000000000000000" + this.state.accountAddr.slice(2);
        let options = {
            fromBlock: 0,
            toBlock: 'latest',
            topics:["0x08a8cea81d39388ee27cc2a7f50558d39a413b44245d345a8264b7353ffa3167",loggedInAddr, null], 
        };

        let filter =  this.state.contractInstance.web3.eth.filter(options);
        filter.get(function(error, result) {
            console.log("Result : ", result);
            console.log("Contract Address : ",CROPS_INSURANCE_CONTRACT_ADDR);
            for(let i = result.length - 1; i >= 0; i--) {
                if(!error && result[i].address == CROPS_INSURANCE_CONTRACT_ADDR) {
                    
                    this.setState({
                        companyId : parseInt(result[result.length - 1].topics[2]),
                        showProfile : true,
                    });
                    break;
                }
            }
        }.bind(this));
    }

    handleSelectionBtn(event) {
        console.log("Value is selected : ", event.target.value);
        this.setState({
            selectedOption : event.target.value,
        });
    }


    render() {
        return (
            <div style={{margin:"0px", textAlign:"center", backgroundImage:`url(${bg1})`, backgroundSize:"cover", backgroundPosition:"center", width:"100%", height:"50vh", zIndex:"-100"}}>
                <h4 style={{marginLeft:"15px", marginTop:"15px", float:"left", color:"white", fontFamily:"'Ubuntu', sans-serif"}}><strong>Oracle Crops Insurance</strong></h4>

                <div style={{ marginTop:"15px", fontFamily:"'Ubuntu', sans-serif", color:"black"}}>
                    <div style={{ fontFamily:"'Yantramanav', sans-serif",marginTop : "20px", marginLeft:"200px", marginRight:"200px", lineHeight:"1.5"}}>
                        <p style={{fontSize:"40px", color:"black"}}> Insurance Company</p>
                    </div>
                    <div style={{fontFamily:"'Yantramanav', sans-serif"}}>
                        <Row className="text-center">
                            <Col md="1"></Col>

                            <Col md="3">
                                <Card className="main-card mb-3" style={{backgroundColor:"transparent"}}>
                                    <CardBody>
                                        {
                                            this.state.showProfile ? 
                                                <button disabled={this.state.metamaskConStatus} value="show_profile" onClick= {(e) => this.handleSelectionBtn(e)} type="button" className="btn btn-primary" style={{ fontSize:"25px", border:"none", width:"250px", height:"220px", float:"left", color:"black", opacity:"0.8", background:"#eaede6"}}><strong>Profile</strong></button>
                                                :
                                                <button disabled={this.state.metamaskConStatus} value="company_registration" onClick= {(e) => this.handleSelectionBtn(e)} type="button" className="btn btn-primary" style={{ fontSize:"25px", border:"none", width:"250px", height:"220px", float:"left", color:"black", opacity:"0.8", background:"#eaede6"}}><strong>Register Your Company</strong></button>
                                        }
                                    </CardBody>   
                                </Card>
                            </Col>

                            <Col md="3">
                                <Card className="main-card mb-3" style={{backgroundColor:"transparent"}}>
                                    <CardBody>
                                        <button disabled={this.state.metamaskConStatus} value="scheme_registration" onClick= {(e) => this.handleSelectionBtn(e)} type="button" className="btn btn-primary" style={{fontSize:"25px", border:"none", width:"250px", height:"220px", float:"right", color:"black", opacity:"0.8", background:"#eaede6"}}><strong>Add Insurance Policy</strong></button>
                                    </CardBody>   
                                </Card>
                            </Col>

                            <Col md="3">
                                <Card className="main-card mb-3" style={{backgroundColor:"transparent"}}>
                                    <CardBody>
                                        <button disabled={this.state.metamaskConStatus} value="scheme_details" onClick= {(e) => this.handleSelectionBtn(e)} type="button" className="btn btn-primary" style={{fontSize:"25px", border:"none", width:"230px", height:"220px", float:"right", color:"black", opacity:"0.8", background:"#eaede6"}}><strong>Policy Details</strong></button>
                                    </CardBody>   
                                </Card>
                            </Col>

                            <Col md="1"></Col>
                        </Row>
                    </div>
                </div>

                <div style={{ fontFamily:"'Ubuntu', sans-serif", marginTop:"50px", color:"black", lineHeight:"1.5", textAlign:"center", backgroundColor:"#e2e3e1"}}>   
                    {
                        this.state.metamaskConStatus ?
                            <Metamask />
                            :
                            null
                    }
                    {
                        this.state.selectedOption == "company_registration" ? 
                            <CompanyRegistrationForm contractInstance={this.state.contractInstance}/>
                            :
                            null
                    }

                    {
                        this.state.selectedOption == "scheme_registration" ?
                            <SchemeRegistrationFrom contractInstance={this.state.contractInstance} companyId={this.state.companyId}/>
                            :
                            null
                    }

                    {
                        this.state.selectedOption == "scheme_details" ?
                            <SchemeDetails contractInstance={this.state.contractInstance} />
                            :
                            null
                    }

                    {
                        this.state.selectedOption == "show_profile" ? 
                            <Profile contractInstance={this.state.contractInstance} companyId={this.state.companyId}/>
                            :
                            null
                    }
                </div>
                

                <div style={{ fontFamily:"'Ubuntu', sans-serif",marginLeft:"200px", marginRight:"200px", marginTop:"100px", color:"black", lineHeight:"1.5", textAlign:"center"}}>   
                    <h4><strong>How it Works?</strong></h4>
                    <p style={{fontSize:"14px", color:"#6c757d"}}>
                        This solution works on weather data like temperature, wind pressure, rainfall, drought, etc.
                        Our smart contract fetch data from <a href="https://www.worldweatheronline.com/"> World Weather Online </a>
                        and calculate loss from these data. Farmer can easily get his/her claim amount through smart contract.
                        You need metamask wallet for interaction.
                    </p>
                    <br/><br/>
                    <div style={{fontFamily:"'Yantramanav', sans-serif"}}>
                        <Row className="text-center">
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Total Farmers
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            50
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Total Companies<br/>
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            5
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Area Coverage<br/>
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            30
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>

            </div>   
                                                     
        )
    }
}