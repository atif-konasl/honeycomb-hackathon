import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle
} from 'reactstrap';
import { Link } from "react-router-dom";

//Background Image
import bg1 from "../../../assets/insurance/homepage_bg.jpeg";




export default class HomePage extends React.Component {
    constructor(props) {
        super(props);
        console.log("In homepage.. ");
    }

    render() {
        return (
            <div style={{margin:"0px", textAlign:"center", backgroundImage:`url(${bg1})`, backgroundSize:"cover", backgroundPosition:"center", width:"100%", height:"100vh", zIndex:"-100"}}>
                <h4 style={{marginLeft:"15px", marginTop:"15px", float:"left", color:"white", fontFamily:"'Ubuntu', sans-serif"}}><strong>Oracle Crops Insurance</strong></h4>

                <div style={{ marginTop:"15px", fontFamily:"'Ubuntu', sans-serif", color:"#65ff36"}}>


                    
                    <div style={{ fontFamily:"'Yantramanav', sans-serif",marginTop : "150px", marginLeft:"200px", marginRight:"200px", lineHeight:"1.5"}}>
                        <p style={{fontSize:"40px"}}> Blockchain Based Crops Insurance Solution.</p>
                        <p style={{fontSize:"14px"}}><strong>We provide a solution for farmers and insurance companies. <br/>
                            This solution can calculate loss according to world weather index standard using Smart-Contract.</strong></p>
                    </div>
                    <br/><br/><br/>
                    <div style={{fontFamily:"'Yantramanav', sans-serif"}}>
                        <Row className="text-center">
                            <Col md="2"></Col>
                            <Col md="8">
                                <Card className="main-card mb-3" style={{backgroundColor:"transparent"}}>
                                    <CardBody>
                                        <Link to={{
                                            pathname: "/hackathon/farmer-corner",
                                            state: {
                                                comapnyAddr : "0xAB..366B"
                                            }
                                        }}>
                                            <button type="button" className="btn btn-primary" style={{ fontSize:"40px", border:"none", width:"300px", height:"250px", float:"left", color:"black", opacity:"0.8", background:"#eaede6"}}><strong>Farmer's Panel</strong></button>
                                        </Link>

                                        <Link to={{
                                            pathname: "/hackathon/insurance-company",
                                            state: {
                                                comapnyAddr : "0xAB..366B"
                                            }
                                        }}>
                                            <button type="button" className="btn btn-primary" style={{fontSize:"40px",width:"300px", border:"none", height:"250px", float:"right", color:"black", opacity:"0.8", background:"#eaede6"}}><strong>Insurance Provider</strong></button>
                                        </Link>
                                        
                                    </CardBody>
                                    <br/><br/>
                                </Card>
                            </Col>
                            <Col md="2"></Col>
                        </Row>

                    </div>
                    
                </div>
                    


                <div style={{ fontFamily:"'Ubuntu', sans-serif",marginLeft:"200px", marginRight:"200px", marginTop:"100px", color:"black", lineHeight:"1.5", textAlign:"center"}}>   
                    <h4><strong>How it Works?</strong></h4>
                    <p style={{fontSize:"14px", color:"#6c757d"}}>
                        This solution works on weather data like temperature, wind pressure, rainfall, drought, etc.
                        Our smart contract fetch data from <a href="https://www.worldweatheronline.com/"> World Weather Online </a>
                        and calculate loss from these data. Farmer can easily get his/her claim amount through smart contract.
                        You need metamask wallet for interaction.
                    </p>
                    <br/><br/>
                    <div style={{fontFamily:"'Yantramanav', sans-serif"}}>
                        <Row className="text-center">
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Total Farmers
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            50
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Total Companies<br/>
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            5
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Area Coverage<br/>
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            50
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>

            </div>   
                                                     
        )
    }
}