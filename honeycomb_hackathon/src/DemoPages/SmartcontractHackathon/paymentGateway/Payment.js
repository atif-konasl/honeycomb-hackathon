import React, {Fragment} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {
    Row, Col,
    Card, CardBody,
    CardTitle, Button, ListGroup, ListGroupItem
} from 'reactstrap';

import PaymentUtil from "../blockchain/payment/Payment";

class Payment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            balance : null,
            account : null,
        }

        this.paymentUtil = new PaymentUtil();
        this.paymentUtil.initMetamask(function(err, result) {
            if(!err) {

                console.log("Metamask is connected successfully");
                this.paymentUtil.getBalanceInLink(function(err, obj) {
                    if(!err) {
                        console.log("Get balance properly : ", obj);
                        this.setState({
                            balance : Number(obj.balance).toFixed(2),
                            account : obj.address,
                        })
                    } else {
                        console.log("Failed to get balance");
                    }
                }.bind(this));
            } else {
                console.log("Metamask is failed to connect");
            }
        }.bind(this));

        this.handleDepositBtn = this.handleDepositBtn.bind(this);
        console.log("In payment : ", this.props.location.state.props);
    }



    handleDepositBtn() {
        this.paymentUtil.sendLinkUsingMetamask(this.props.location.state.regData.depositAmount, function(err, txHash) {
            if(!err) {
                console.log("Successfully submited transaction", txHash);
                this.props.history.push({
                    pathname : "/hackathon/status",
                    state : {
                        txHash : txHash,
                        props : this.props.location.state.props,
                        regData : this.props.location.state.regData
                    }
                });
            } else {
                console.log("Failed to send transaction ", err);
            }
        }.bind(this));
    }

    render() {
        let showInSelectBar = "Connect MetaMask";
        let depositeValue = "-";
        if(this.state.account) {
            showInSelectBar = this.state.account.substring(0,4) + ".." + this.state.account.slice(this.state.account.length - 4);
            showInSelectBar = 'MetaMask ' + showInSelectBar.toUpperCase();
        }

        if (this.props.location.state) {
            depositeValue = this.props.location.state.regData.depositAmount;
        }
         return (
            <Fragment>
                <ReactCSSTransitionGroup
                    component="div"
                    transitionName="TabsAnimation"
                    transitionAppear={true}
                    transitionAppearTimeout={0}
                    transitionEnter={false}
                    transitionLeave={false}>
                    <div>
                        <Row>
                            <Col md="12">
                                <br/><br/>
                                <Card className="main-card" style={{marginLeft:"17%", padding:"5px", width:"700px", height:"290px", border:"0.5px solid gray"}}>
                                    <CardBody>
                                        <p style={{textAlign:"left", color:"black"}}><strong>1. Connect Your Wallet</strong></p>
                                        <div className="row" >

                                            <div className="col-sm-8">
                                                <select style={{ fontSize:"12px", width:"280px", height:"35px", backgroundColor:"ligthGray"}}>
                                                    <option value="" disabled selected>Connect</option>
                                                    <option value="1">{showInSelectBar}</option>
                                                    <option value="2">Connect Web3</option>
                    
                                                </select>
                                              
                                            </div>
                                            <div className="col-sm-4">
                                                <span>Available LINK</span><br/>
                                                {this.state.balance ? <span>{this.state.balance}</span> : <span>-</span>}
                                            </div>
                                        </div>
                                        <br/>
                                        <br/>
                                        <p style={{textAlign:"left", color:"black"}}><strong>2. Your deposite money</strong></p>
                                        <input type="text" style={{width:"100%", height:"38px", textAlign: "center", color:"black"}}
                                                value={depositeValue} disabled/>
                                         <button onClick={this.handleDepositBtn} type="button" className="btn btn-primary" style={{marginTop:"10px",width:"100%"}}>Deposite</button>
                                 
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </ReactCSSTransitionGroup>
            </Fragment>
        )
    }
}

export default Payment;