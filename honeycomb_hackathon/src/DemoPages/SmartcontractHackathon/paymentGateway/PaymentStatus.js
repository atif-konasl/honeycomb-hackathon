import React, {Fragment} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {
    Row, Col,
    Card, CardBody,
    CardTitle, Button, ListGroup, ListGroupItem
} from 'reactstrap';

import loadingGif from '../../../assets/betting-logo/Loading_icon.gif'
import PaymentUtil from "../blockchain/payment/Payment";

class PaymentStatus extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props.location.state);

        this.state = {
            status : "pending",
            statusCls : "badge badge-warning",
        }

        this.showErrorDiv = this.showErrorDiv.bind(this);
        this.showSuccessDiv = this.showSuccessDiv.bind(this);
        this.registerData = this.registerData.bind(this);

        this.paymentUtil = new PaymentUtil();
        this.paymentUtil.initMetamask(function(err, result) {
            if(!err) {
                console.log("Metamask is connected successfully", this.props.location.state.txHash);
                let txHash = this.props.location.state.txHash;
    
                this.paymentUtil.getTxConfirmation(txHash, function(err, status) {
                    if(!err) {
                        if(status == '0x1') {
                            console.log("Payment successful.");
                            this.setState({
                                status : "success",
                                statusCls : "badge badge-success"
                            })
                            this.showSuccessDiv();
                        } else {
                            console.log("Payment failed");
                            this.setState({
                                status : "failed",
                                statusCls : "badge badge-danger"
                            })
                            this.showErrorDiv();
                        }
                    }
                }.bind(this));
            } else {
                console.log("Metamask is failed to connect");
            }
        }.bind(this));
    }


    registerData() {

        const regData = this.props.location.state.regData;
        this.paymentUtil.registerCompany(regData.companyName, regData.license, regData.address, regData.des, regData.depositAmount, function(err, txHash) {
            if(!err) {
                this.paymentUtil.getTxConfirmation(txHash, function(err, status) {
                    if(!err) {
                        if(status == '0x1') {
                            console.log("Successfully Registered your company");
                        } else {
                            console.log("Company registration is failed");
                        }
                    } else {

                    }
                }.bind(this))
            } else {

            }
        }.bind(this));
    }



    showSuccessDiv () {
        document.getElementById('status_div').innerHTML = `
                <h7 style={{color:"black"}}>
                    <strong>
                        🎉 Success! Transaction successfully submitted!
                    </strong>
                </h7>
                <br/>
                <p style={{fontSize:"13px", color:"black"}}>
                    Your transaction will be finalized in a few minutes.
                </p>  
        `;
    }
    
    showErrorDiv () {
        document.getElementById('status_div').innerHTML = `
            <h7 style={{color:"black"}}>
                <strong>
                    ⚠️Oops, there's been an error!
                </strong></h7>
            <br/>
            <p style={{fontSize:"13px", color:"black"}}>
                Refresh this page to go back to the beginning.
            </p>
        `;
    }

    

    render() {
        return (
            <Fragment>
                <ReactCSSTransitionGroup
                    component="div"
                    transitionName="TabsAnimation"
                    transitionAppear={true}
                    transitionAppearTimeout={0}
                    transitionEnter={false}
                    transitionLeave={false}>
                    <Row>
                        <Col md="12">
                            <br/><br/>
                            <Card className="main-card" style={{marginLeft:"17%", width:"700px", height:"290px", border:"0.5px solid gray"}}>
                                <CardBody>
                                    <div id="status_div" style={{padding:"5px 10px 5px 10px", height:"60px", backgroundColor:"#f8f8f9", border:"0.5px solid green"}} >
                                        {/* <img src={loadingGif} width="50" height="60" className="d-inline-block align-top" alt=""/> */}
                                        <h7 style={{color:"black"}}><strong>Awaiting confirmation from your Ledger wallet...</strong></h7>
                                        <br/>
                                        <p style={{fontSize:"13px", color:"black"}}>Please confirm the transaction on your Ledger wallet.</p>
                                    </div>

                                    <div style={{padding:"5px 10px 5px 10px", marginTop: "10px", height:"190px", backgroundColor:"#f8f8f9", border:"0.5px solid green"}} >
                                        <div style={{textAlign : "center", marginTop:"55px"}}>
                                                {this.props.location.state ? this.props.location.state.regData.depositAmount : "- "} LINK    
                                        </div>

                                    
                                        <div className={this.state.statusCls} style={{ marginTop:"7px", marginLeft:"45%"}}>
                                            {this.state.status}
                                        </div> 

                                        {this.state.status == "success" ? 

                                            // <p style={{fontSize:"13px", color:"black"}} >Your initail deposite is successfull. Now you need to register your company data.</p>
                                            <div style={{margin:"10%", marginTop:"10px"}}>
                                                <button onClick={this.registerData} type="button" className="btn btn-primary" style={{marginTop:"10px",width:"100%"}}>Register Company Data</button>
                                            </div> 
                                                : 
                                                null
                                        }
                                        
                                        
                                    </div>
                                
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </ReactCSSTransitionGroup>
            </Fragment>
        )
    }
}

export default PaymentStatus;