#!/bin/bash
#rm -rf ./Output/*
#rm -rf ../abiData/*.js
#mkdir ./Output/js

for filename in ./AllContracts/*.sol; do
	
	
	
	
	contractnameWithEx=$(basename "$filename")
	contractname="${contractnameWithEx%.*}"
	echo "$contractname"
	dirpath="./Output/$contractname"
	jsPath="./Output/js"
	mkdir -p $dirpath
	mkdir -p $jsPath
	./compile_parse_contract.sh "$filename" "$dirpath"  "./solc-static-linux"
	
	cd $dirpath	
	
	#abi=$(cat ./*.abi)
	#data=$(cat ./*.bin)
	#echo "$abi"
	#echo "$data"
	
	
	cd ../..
	echo -e "var $contractname = { \n abi: function(){ \n return $(head -n 1000 $dirpath/$contractname.abi) \n}, \n data:function() { \n return \"0x$(head -n 1000 $dirpath/$contractname.bin)\"  \n}, \n}\n" > ./$jsPath/$contractname.js
	echo "export default $contractname;" >> ./$jsPath/$contractname.js
done 

#cp ./Output/js/*.js ../abiData/
