pragma solidity 0.4.24;

import "https://github.com/smartcontractkit/chainlink/evm/contracts/ChainlinkClient.sol";
import "https://github.com/smartcontractkit/chainlink/evm/contracts/vendor/Ownable.sol";

contract CropsInsurance is ChainlinkClient, Ownable {
    
    uint256 constant private ORACLE_PAYMENT = LINK / 10;
    uint256 constant private TOTAL_REQ = 20;
    uint256 constant private LOSS_BASE_SD = 3;
    uint256 constant private DATA_SET_SIZE = 10;
    
    
    event CompanyRegistrationEvent (
        address indexed companyAddr,
        uint256 indexed companyId
    );
    
    event AddSchemeEvent (
        address indexed companyAddr,
        uint256 indexed companyId,
        uint256 indexed schemeId
    );
    
    event RegistrationInSchemeEvent (
        address indexed farmerAddr,
        uint256 indexed companyId,
        uint256 indexed farmerId
    );
    
    enum SchemeState {
        ACTIVE,
        IN_ACTIVE
    }
    
    enum ClaimStatus {
        NOT_CLAIMED,
        IN_REQUEST,
        IN_PROCESSING,
        IN_MONEY_DISBURSMENT,
        FINISHED
    }
    
    
    struct InsuranceScheme {
        string          schemeName;
        string          cropsName;
        string          location;
        uint256         schemePrice;
        string          startDate;
        string          endDate;
        
        SchemeState     state;
    }
    
    struct FarmerInfo {
        string          name;
        string          location;
        uint256         balance;
        uint256         schemeId;

        ClaimData       claimData;
        
    }
    
    struct InsuranceCompany {
        string                  companyName;
        string                  companyDes;
        string                  companyAddr;
        uint256                 totalCapital;
        
        uint256                 totalSchemeCount;
        uint256                 totalRegFarmersCount;
        uint256                 totalPaid;
        
        mapping (uint256 => FarmerInfo) registeredFarmers;
        mapping (uint256 => InsuranceScheme) schemes;
        
        mapping (uint256 => uint256) schemeFarmerCounts;
       
    }
    
    
    
    
    struct ClaimData {
        mapping (uint256 => uint256[]) temps;
        mapping (uint256 => uint256[]) winds;
        
        uint256         standardDeviationTemp;
        uint256         standardDeviationWind;
        uint256         loss;
        ClaimStatus     status;
        
    }
    
    
    struct RequestorInfo {
        address         requestorAddr;
        uint256         companyId;
        uint256         farmerId;
        
        uint256         factorId;
        uint256         responseCount;
    }
    
    
    mapping (uint256 => InsuranceCompany) companies;
    uint256         public      totalCompanies;
    uint256         public      totalInsurancePolicies;
    uint256         public      totalRegisterdFarmers;
    
    // For Oracle
    bytes32   public jobId;
    address   public oracle;
    
    
    
    //--------------- For Request -------------------------
    mapping (uint256 => string) paths;
    mapping (bytes32 => RequestorInfo) requestMapping;
    mapping (address => RequestorInfo) responseMapping;
 

    
    constructor(
        address     _link,
        address     _oracle,
        bytes32     _jobId
    ) 
        public 
        Ownable() 
    {
        setChainlinkToken(_link);
        updateRequestDetails(_oracle, _jobId);
        
        paths[0] = "data.weather.0.hourly.0.tempC";
        paths[1] = "data.weather.0.hourly.0.windspeedKmph";
        
        paths[2] = "data.weather.1.hourly.0.tempC";
        paths[3] = "data.weather.1.hourly.0.windspeedKmph";
        
        paths[4] = "data.weather.2.hourly.0.tempC";
        paths[5] = "data.weather.2.hourly.0.windspeedKmph";
        
        paths[6] = "data.weather.3.hourly.0.tempC";
        paths[7] = "data.weather.3.hourly.0.windspeedKmph";
        
        paths[8] = "data.weather.4.hourly.0.tempC";
        paths[9] = "data.weather.4.hourly.0.windspeedKmph";
        
        paths[10] = "data.weather.5.hourly.0.tempC";
        paths[11] = "data.weather.5.hourly.0.windspeedKmph";
        
        paths[12] = "data.weather.6.hourly.0.tempC";
        paths[13] = "data.weather.6.hourly.0.windspeedKmph";
        
        paths[14] = "data.weather.7.hourly.0.tempC";
        paths[15] = "data.weather.7.hourly.0.windspeedKmph";
        
        paths[16] = "data.weather.8.hourly.0.tempC";
        paths[17] = "data.weather.8.hourly.0.windspeedKmph";
        
        paths[18] = "data.weather.9.hourly.0.tempC";
        paths[19] = "data.weather.9.hourly.0.windspeedKmph";
    }
    
    
    function updateRequestDetails(
        address     _oracle,
        bytes32     _jobId
     )
        public
        onlyOwner()
    {
        jobId = _jobId;
        oracle = _oracle;
    }

    
 
 
 //----------------------------------------------------- REQUEST  STARTS -----------------------------//
 
 
//  function mockRequestTest() public {
//     InsuranceCompany memory company;
//     company.companyName = "Abc";
//     company.totalCapital = 10 * LINK;
    
//     companies[0] = company;
//     totalCompanies++;
    
    
//     InsuranceScheme memory scheme;
//     scheme.cropsName = "Rice";
//     scheme.location = "Dhaka";
//     scheme.state = SchemeState.ACTIVE;
//     scheme.schemePrice = 2 * LINK;
//     scheme.startDate = "2019-11-01";
//     scheme.endDate = "2019-11-30";
    
//     companies[0].schemes[0] = scheme;
//     companies[0].totalSchemeCount++;
    
//     FarmerInfo memory farmerInfo;
//     farmerInfo.balance = 2 * LINK;
//     farmerInfo.schemeId = 0;
//     farmerInfo.claimData.status = ClaimStatus.NOT_CLAIMED;
    
//     companies[0].registeredFarmers[0] = farmerInfo;
//     companies[0].totalRegFarmersCount++;
    
//     farmerInfo.balance = 2 * LINK;
//     farmerInfo.schemeId = 0;
//     farmerInfo.claimData.status = ClaimStatus.NOT_CLAIMED;
    
//     companies[0].registeredFarmers[1] = farmerInfo;
//     companies[0].totalRegFarmersCount++;
//  }
    
    
    
    function claimRequest(uint256 _companyId, uint256 _schemeId, uint256 _farmerId) 
        external 
    {
        companies[_companyId].registeredFarmers[_farmerId].claimData.status = ClaimStatus.IN_REQUEST;
        
        string memory startDate = companies[_companyId].schemes[_schemeId].startDate;
        string memory endDate = companies[_companyId].schemes[_schemeId].endDate;
        string memory location = companies[_companyId].schemes[_schemeId].location;
        
        Chainlink.Request memory request;
        bytes32 requestId;
        
        RequestorInfo memory requestor;
        
        requestor.companyId = _companyId;
        requestor.farmerId = _farmerId;
        requestor.requestorAddr = msg.sender;
        
        responseMapping[msg.sender] = requestor;
        
        for (uint i = 0; i < TOTAL_REQ; i++) {
            request = buildChainlinkRequest(jobId, this, this.getResponse.selector);
            
            request.add("q", location);
            request.add("date", startDate);
            request.add("enddate", endDate);
            request.add("tp", "24");
            request.add("copyPath", paths[i]);
            
            requestId = sendChainlinkRequestTo(oracle, request, ORACLE_PAYMENT);
            
            if(i % 2 == 0) {
                requestor.factorId = 0;
            } else {
                requestor.factorId = 1;
            }
            
            requestMapping[requestId] = requestor;
        }
    }
 

    function getResponse(bytes32 _requestId, int256 _result)
        public
        recordChainlinkFulfillment(_requestId)
    {
        RequestorInfo memory requestor = requestMapping[_requestId];
        
        address requestorAddr = requestor.requestorAddr;
        uint256 companyId = requestor.companyId;
        uint256 farmerId = requestor.farmerId;
        
        if(requestor.factorId == 0) {
             companies[companyId].registeredFarmers[farmerId].claimData.temps[farmerId].push(uint256(_result));
        } else {
            companies[companyId].registeredFarmers[farmerId].claimData.winds[farmerId].push(uint256(_result));
        }
        
        delete requestMapping[_requestId];
        responseMapping[requestorAddr].responseCount += 1;
        
        if(responseMapping[requestorAddr].responseCount == TOTAL_REQ) {
            companies[companyId].registeredFarmers[farmerId].claimData.status = ClaimStatus.IN_PROCESSING;
            calculateSD(companyId, farmerId);
            delete responseMapping[requestorAddr];
        }
    }
    
 
    
    
    function getWind(uint256 _companyId, uint256 _farmerId, uint256 _index) 
        public 
        view 
        returns (uint256, uint256, uint256)
    {
        return (
            _farmerId,
            _index,
            companies[_companyId].registeredFarmers[_farmerId].claimData.winds[_farmerId][_index]
        );
    }
    
    
    function getTemperature(uint256 _companyId, uint256 _farmerId, uint256 _index) 
        public 
        view 
        returns (uint256, uint256, uint256)
    {
        return (
            _farmerId,
            _index,
            companies[_companyId].registeredFarmers[_farmerId].claimData.temps[_farmerId][_index]
        );
    }
    
    
    function getClaimStatus(uint256 _companyId, uint256 _farmerId) 
        public
        view
        returns (uint256, uint256) 
    {
        return (
            _farmerId,
            uint256(companies[_companyId].registeredFarmers[_farmerId].claimData.status)
        );
    }
    
    
     //----------------------------------------------------- REQUEST  END -----------------------------//
     
     
     //---------------------------------------- Standard Deviation Calculation --------------//
    
    
    uint256 public sumSquareTemp;
    uint256 public sumSquareWind;
    uint256 public standardDeviationTemp;
    uint256 public standardDeviationWind;
    uint256 public loss;
    
    // Only mock purpose//
    
    // function mock(uint256 _companyId, uint256 _schemeId, uint256 _farmerId) external {
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.temps[msg.sender].push(31);
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.temps[msg.sender].push(25);
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.temps[msg.sender].push(28);
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.temps[msg.sender].push(20);
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.temps[msg.sender].push(15);
        
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.winds[msg.sender].push(1);
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.winds[msg.sender].push(9);
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.winds[msg.sender].push(5);
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.winds[msg.sender].push(3);
    //     companies[_companyId].schemes[_schemeId].registeredFarmers[_farmerId].claimData.winds[msg.sender].push(31);
    // }
        
    function calculateSD(uint256 _companyId, uint256 _farmerId) public {
        
        
        uint256 sumTemp = 0;
        uint256 sumWind = 0;
        
        
        for(uint256 i = 0; i < DATA_SET_SIZE; i++) {
            sumTemp += companies[_companyId].registeredFarmers[_farmerId].claimData.temps[_farmerId][i];
            sumWind += companies[_companyId].registeredFarmers[_farmerId].claimData.winds[_farmerId][i];
        }
        
        uint256 meanTemp = sumTemp / DATA_SET_SIZE;
        uint256 meanWind = sumWind / DATA_SET_SIZE;

        
        for(uint256 j = 0; j < DATA_SET_SIZE; j++) {
            uint256 temp = companies[_companyId].registeredFarmers[_farmerId].claimData.temps[_farmerId][j];
            uint256 diff = 0;
            if(meanTemp >= temp) {
                diff = meanTemp - temp;
            } else {
                diff = temp - meanTemp;
            }
            sumSquareTemp += diff**2;
        }
        
        for(uint256 k = 0; k < DATA_SET_SIZE; k++) {
            uint256 wind = companies[_companyId].registeredFarmers[_farmerId].claimData.winds[_farmerId][k];
            diff = 0;
            if(meanWind >= wind) {
                diff = meanWind - wind;
            } else {
                diff = wind - meanWind;
            }
            sumSquareWind += diff**2;
        }
        
        standardDeviationTemp = sqrt(sumSquareTemp / (DATA_SET_SIZE - 1));
        standardDeviationWind = sqrt(sumSquareWind / (DATA_SET_SIZE - 1));
        
        
        companies[_companyId].registeredFarmers[_farmerId].claimData.standardDeviationTemp = standardDeviationTemp;
        companies[_companyId].registeredFarmers[_farmerId].claimData.standardDeviationWind = standardDeviationWind;
        
        
        loss = (standardDeviationWind + standardDeviationTemp) / 2;
        
        if(loss > LOSS_BASE_SD) {
            loss = (loss - LOSS_BASE_SD) * 5;
            companies[_companyId].registeredFarmers[_farmerId].claimData.loss = loss * LINK;
        }
        companies[_companyId].registeredFarmers[_farmerId].claimData.status = ClaimStatus.IN_MONEY_DISBURSMENT;
    }
    
    function sqrt(uint x) public pure returns (uint y) {
        uint z = (x + 1) / 2;
        y = x;
        while (z < y) {
            y = z;
            z = (x / z + z) / 2;
        }
        return (y);
    }
    
    //----------------------- Finish Standard Deviation Calculation -----------------------//
    
    
    
    
    function withdrawClimedLink (uint256 _companyId, uint256 _farmerId) public {
        
        uint256 depositBalance = companies[_companyId].registeredFarmers[_farmerId].balance;
        uint256 payableAmount = companies[_companyId].registeredFarmers[_farmerId].claimData.loss;
        companies[_companyId].totalPaid += payableAmount;
        
        uint256 totalPayable = depositBalance + payableAmount;
        
        companies[_companyId].registeredFarmers[_farmerId].balance = 0;
        companies[_companyId].totalCapital = companies[_companyId].totalCapital - payableAmount;
        
        companies[_companyId].registeredFarmers[_farmerId].claimData.status = ClaimStatus.FINISHED;
        
        LinkTokenInterface link = LinkTokenInterface(chainlinkTokenAddress());
        require(link.transfer(msg.sender, totalPayable), "Unable to transfer");
    }
    
    
    

    
    // -----------------------------Start Registering Data -------------------------------//
    function registerCompany (
        string _companyName, string _companyDes, 
        string _companyAddr, uint256 _depositedAmount
    ) 
    public
    {
        InsuranceCompany memory company;
        
        company.companyName = _companyName;
        company.companyDes = _companyDes;
        company.companyAddr = _companyAddr;
        company.totalCapital = _depositedAmount * LINK;
        
        companies[totalCompanies] = company;
        
        emit CompanyRegistrationEvent(msg.sender, totalCompanies);
        totalCompanies += 1;
    }
    
    
    function addScheme (
        uint256 _companyId, string _schemeName, string _cropsName, string _location,
        uint256 _schemePrice, string _startDate, string _endDate
    ) public  
    
    {
        
        InsuranceScheme memory scheme;
        
        scheme.schemeName = _schemeName;
        scheme.cropsName = _cropsName;
        scheme.location = _location;
        scheme.schemePrice = _schemePrice;
        scheme.startDate = _startDate;
        scheme.endDate = _endDate;
        scheme.state = SchemeState.ACTIVE;
        
        uint256 totalScheme = companies[_companyId].totalSchemeCount;
        companies[_companyId].schemes[totalScheme] = scheme;
        
        emit AddSchemeEvent(msg.sender, _companyId, totalScheme);
        
        companies[_companyId].totalSchemeCount += 1;
        totalInsurancePolicies++;
    } 
    
    
    function registerInScheme (
        string _name, string _addr, uint256 _companyId, uint256 _schemeId, uint256 _deposite
        
        ) public 
    {
        
        
        FarmerInfo memory farmerInfo;
        farmerInfo.name = _name;
        farmerInfo.location = _addr;
        farmerInfo.balance = _deposite;
        farmerInfo.schemeId = _schemeId;
        farmerInfo.claimData.status = ClaimStatus.NOT_CLAIMED;
        
        uint256 totalRegFarmersCount = companies[_companyId].totalRegFarmersCount;
        companies[_companyId].registeredFarmers[totalRegFarmersCount] = farmerInfo;
        
        emit RegistrationInSchemeEvent(msg.sender, _companyId, totalRegFarmersCount);
        
        uint256 totalRegFarmersInScheme = companies[_companyId].schemeFarmerCounts[_schemeId];
        companies[_companyId].schemeFarmerCounts[_schemeId] = totalRegFarmersInScheme + 1;
        
        companies[_companyId].totalRegFarmersCount += 1;
        totalRegisterdFarmers += 1;
        
    }
    
    
    //----------------------------------- End Registering Data ------------------------//
    
    
    
    
    function getCompanyInfo (uint256 _companyId) 
        public 
        view
        
        returns (
            string,
            string,
            string,
            uint256,
            uint256,
            uint256
        ) 
    {
        return (
            companies[_companyId].companyName,
            companies[_companyId].companyDes,
            companies[_companyId].companyAddr,
            companies[_companyId].totalCapital,
            companies[_companyId].totalSchemeCount,
            companies[_companyId].totalRegFarmersCount
        );
    }

    function getSchemeCount (uint256 _companyId) 
        public
        view
        returns (uint256)
    {
        return (
            companies[_companyId].totalSchemeCount
        );
    }
    
    
    function getSchemeInfo (uint256 _companyId, uint256 _schemeId)
        public 
        view
        
        returns (
            uint256,
            string,
            string,
            string,
            uint256,
            string
        )
    {
        return (
            _schemeId,
            companies[_companyId].schemes[_schemeId].schemeName,
            companies[_companyId].schemes[_schemeId].cropsName,
            companies[_companyId].schemes[_schemeId].location,
            companies[_companyId].schemes[_schemeId].schemePrice,
            companies[_companyId].schemes[_schemeId].startDate
        );
        
    }
    
        

        
    function getFarmerInfo(uint256 _companyId, uint256 _farmerId)
        public
        view
        returns (
            uint256,
            uint256,
            uint256,
            uint256,
            uint256,
            uint256
        )
    {
        return (
            companies[_companyId].registeredFarmers[_farmerId].balance,
            companies[_companyId].registeredFarmers[_farmerId].schemeId,
            companies[_companyId].registeredFarmers[_farmerId].claimData.standardDeviationTemp,
            companies[_companyId].registeredFarmers[_farmerId].claimData.standardDeviationWind,
            companies[_companyId].registeredFarmers[_farmerId].claimData.loss,
            uint256(companies[_companyId].registeredFarmers[_farmerId].claimData.status)
        );
    }
    
    

    
    function withdrawLink() public {
        LinkTokenInterface link = LinkTokenInterface(chainlinkTokenAddress());
        require(link.transfer(msg.sender, link.balanceOf(address(this))), "Unable to transfer");
    }
    
    
    
    function getBalance() public returns(uint256) {
        return LinkTokenInterface(chainlinkTokenAddress()).balanceOf(msg.sender);
    }
        
        
    function getPieChartData(uint256 _companyId) 
        public 
        view 
        returns(
            uint256, 
            uint256, 
            uint256
        ) 
    {
        return (
            companies[_companyId].totalSchemeCount,
            companies[_companyId].totalRegFarmersCount,
            companies[_companyId].totalPaid
        );
    }
    
    function getRegFarmerCountInScheme(uint256 _companyId, uint256 _schemeId)
        public
        view 
        returns(uint256, uint256) 
    {
        return (
            _schemeId,
            companies[_companyId].schemeFarmerCounts[_schemeId]
        );
    }

}

