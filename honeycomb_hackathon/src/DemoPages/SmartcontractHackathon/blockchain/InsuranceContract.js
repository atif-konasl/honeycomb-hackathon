import Web3 from 'web3';

//Constant
const CHAIN_LINK_ABI = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"},{"name":"_data","type":"bytes"}],"name":"transferAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_subtractedValue","type":"uint256"}],"name":"decreaseApproval","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_addedValue","type":"uint256"}],"name":"increaseApproval","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"data","type":"bytes"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"}];
const CROPS_INSURANCE_CONTRACT_ABI = [
	{
		"constant": true,
		"inputs": [],
		"name": "standardDeviationWind",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_schemeId",
				"type": "uint256"
			}
		],
		"name": "getSchemeInfo",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "getBalance",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "sumSquareTemp",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			}
		],
		"name": "getSchemeCount",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "totalCompanies",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "totalInsurancePolicies",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_schemeId",
				"type": "uint256"
			}
		],
		"name": "getRegFarmerCountInScheme",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_farmerId",
				"type": "uint256"
			}
		],
		"name": "withdrawClimedLink",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_farmerId",
				"type": "uint256"
			},
			{
				"name": "_index",
				"type": "uint256"
			}
		],
		"name": "getWind",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "x",
				"type": "uint256"
			}
		],
		"name": "sqrt",
		"outputs": [
			{
				"name": "y",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "pure",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "renounceOwnership",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_farmerId",
				"type": "uint256"
			}
		],
		"name": "getClaimStatus",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "loss",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "sumSquareWind",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			}
		],
		"name": "getCompanyInfo",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "oracle",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_schemeId",
				"type": "uint256"
			},
			{
				"name": "_farmerId",
				"type": "uint256"
			}
		],
		"name": "claimRequest",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "owner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "withdrawLink",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "standardDeviationTemp",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_oracle",
				"type": "address"
			},
			{
				"name": "_jobId",
				"type": "bytes32"
			}
		],
		"name": "updateRequestDetails",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_farmerId",
				"type": "uint256"
			}
		],
		"name": "getFarmerInfo",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "jobId",
		"outputs": [
			{
				"name": "",
				"type": "bytes32"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_farmerId",
				"type": "uint256"
			},
			{
				"name": "_index",
				"type": "uint256"
			}
		],
		"name": "getTemperature",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_requestId",
				"type": "bytes32"
			},
			{
				"name": "_result",
				"type": "int256"
			}
		],
		"name": "getResponse",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_schemeName",
				"type": "string"
			},
			{
				"name": "_cropsName",
				"type": "string"
			},
			{
				"name": "_location",
				"type": "string"
			},
			{
				"name": "_schemePrice",
				"type": "uint256"
			},
			{
				"name": "_startDate",
				"type": "string"
			},
			{
				"name": "_endDate",
				"type": "string"
			}
		],
		"name": "addScheme",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_farmerId",
				"type": "uint256"
			}
		],
		"name": "calculateSD",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "totalRegisterdFarmers",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_name",
				"type": "string"
			},
			{
				"name": "_addr",
				"type": "string"
			},
			{
				"name": "_companyId",
				"type": "uint256"
			},
			{
				"name": "_schemeId",
				"type": "uint256"
			},
			{
				"name": "_deposite",
				"type": "uint256"
			}
		],
		"name": "registerInScheme",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_companyId",
				"type": "uint256"
			}
		],
		"name": "getPieChartData",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_newOwner",
				"type": "address"
			}
		],
		"name": "transferOwnership",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_companyName",
				"type": "string"
			},
			{
				"name": "_companyDes",
				"type": "string"
			},
			{
				"name": "_companyAddr",
				"type": "string"
			},
			{
				"name": "_depositedAmount",
				"type": "uint256"
			}
		],
		"name": "registerCompany",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"name": "_link",
				"type": "address"
			},
			{
				"name": "_oracle",
				"type": "address"
			},
			{
				"name": "_jobId",
				"type": "bytes32"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "companyAddr",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "companyId",
				"type": "uint256"
			}
		],
		"name": "CompanyRegistrationEvent",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "companyAddr",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "companyId",
				"type": "uint256"
			},
			{
				"indexed": true,
				"name": "schemeId",
				"type": "uint256"
			}
		],
		"name": "AddSchemeEvent",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "farmerAddr",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "companyId",
				"type": "uint256"
			},
			{
				"indexed": true,
				"name": "farmerId",
				"type": "uint256"
			}
		],
		"name": "RegistrationInSchemeEvent",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "previousOwner",
				"type": "address"
			}
		],
		"name": "OwnershipRenounced",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "previousOwner",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "newOwner",
				"type": "address"
			}
		],
		"name": "OwnershipTransferred",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "id",
				"type": "bytes32"
			}
		],
		"name": "ChainlinkRequested",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "id",
				"type": "bytes32"
			}
		],
		"name": "ChainlinkFulfilled",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "id",
				"type": "bytes32"
			}
		],
		"name": "ChainlinkCancelled",
		"type": "event"
	}
];

const CHAIN_LINK_ADDR = "0x20fE562d797A42Dcb3399062AE9546cd06f63280";
const CROPS_INSURANCE_CONTRACT_ADDR = "0x28d93e988f703103e0b8c2c721d001055ba282ce";

export default function InsuranceContract () {

    this.web3 = null;
    this.loggedInAccount = null;
    this.token = null;
    this.tokenInstance = null;
    this.contract = null;
	this.contractInstance = null;
	

    
    Object.defineProperty (this, 'initMetamask', {
        value : async function (callback) {
            if (typeof window.ethereum !== 'undefined') {
                this.web3 = new Web3(window.ethereum);
                try {
                    await window.ethereum.enable();
                    this.loggedInAccount = this.web3.eth.accounts[0];
                    this.token = this.web3.eth.contract(CHAIN_LINK_ABI);
                    this.tokenInstance = this.token.at(CHAIN_LINK_ADDR);

                    this.contract = this.web3.eth.contract(CROPS_INSURANCE_CONTRACT_ABI);
                    this.contractInstance = this.contract.at(CROPS_INSURANCE_CONTRACT_ADDR);
                    callback(null, true);
                    
                } catch (error) {
                    console.log("User denied account access.", error);
                    callback(error, null);

                }
            } else if (window.web3 !== null) {
                this.web3 = new Web3(window.web3.currentProvider);
                this.loggedInAccount = this.web3.eth.accounts[0];
                this.token = this.web3.eth.contract(CHAIN_LINK_ABI);
                this.tokenInstance = this.token.at(CHAIN_LINK_ADDR);

                this.contract = this.web3.eth.contract(CROPS_INSURANCE_CONTRACT_ABI);
                this.contractInstance = this.contract.at(CROPS_INSURANCE_CONTRACT_ADDR);
                callback(null, true);

            } else {
                callback(new Error("Metamask is not installed in browser. Please add it first in your browser"), null);
                
            }
        },
        enumerable: true,
        configurable: true
	});



	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	// -------------------------------------------- SETTER FUNCTION STARTS---------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//


	Object.defineProperty (this, 'registerCompany', {
        value : function (_companyName, _description, _address, _depositedAmount, callback) {
            
            if(this.contractInstance) {
                console.log("Going register a company : ", _companyName, _description, _address, _depositedAmount);
                this.contractInstance.registerCompany (_companyName, _description, _address, _depositedAmount, 
                {
                    from : this.loggedInAccount,
                    gas:  "6000000",
                },
                function (err, txHash) {
                    if (txHash !== 'undefined') {
                        callback(null, txHash);
                    } else {
                        console.log('Error : ' + err);
                        callback(err, null);
                    }
                }.bind(this))
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});
	

	Object.defineProperty (this, 'addScheme', {
        value : function (_companyId, _schemeName, _cropsName, _location, _schemePrice, _startDate, _endDate, callback) {
            
            if(this.contractInstance) {
                console.log("Going register a scheme : ", _companyId, _schemeName, _cropsName, _location, _schemePrice, _startDate, _endDate);
                this.contractInstance.addScheme (_companyId, _schemeName, _cropsName, _location, _schemePrice, _startDate, _endDate, 
                {
                    from : this.loggedInAccount,
                    gas:  "6000000",
                },
                function (err, txHash) {
                    if (txHash !== 'undefined') {
                        callback(null, txHash);
                    } else {
                        console.log('Error : ' + err);
                        callback(err, null);
                    }
                }.bind(this))
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});

	Object.defineProperty (this, 'registerInScheme', {
        value : function (_name, _address, _companyId, _schemeId, _depositedAmount, callback) {
            
            if(this.contractInstance) {
                console.log("Going register a company : ", _name, _address, _companyId, _schemeId, _depositedAmount,);
                this.contractInstance.registerInScheme (_name, _address, _companyId, _schemeId, _depositedAmount, 
                {
                    from : this.loggedInAccount,
                    gas:  "6000000",
                },
                function (err, txHash) {
                    if (txHash !== 'undefined') {
                        callback(null, txHash);
                    } else {
                        console.log('Error : ' + err);
                        callback(err, null);
                    }
                }.bind(this))
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});
	

	Object.defineProperty (this, 'claimRequest', {
        value : function (_companyId, _schemeId, _farmerId, callback) {
            
            if(this.contractInstance) {
                console.log("Going register a scheme : ", _companyId, _schemeId, _farmerId);
                this.contractInstance.claimRequest (_companyId, _schemeId, _farmerId, 
                {
                    from : this.loggedInAccount,
                    gas:  "6000000",
                },
                function (err, txHash) {
                    if (txHash !== 'undefined') {
                        callback(null, txHash);
                    } else {
                        console.log('Error : ' + err);
                        callback(err, null);
                    }
                }.bind(this))
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});


	Object.defineProperty (this, 'withdrawClimedLink', {
        value : function (_companyId, _farmerId, callback) {
            
            if(this.contractInstance) {
                console.log("Going withdraw : ", _companyId, _farmerId);
                this.contractInstance.withdrawClimedLink (_companyId, _farmerId, 
                {
                    from : this.loggedInAccount,
                    gas:  "6000000",
                },
                function (err, txHash) {
                    if (txHash !== 'undefined') {
                        callback(null, txHash);
                    } else {
                        console.log('Error : ' + err);
                        callback(err, null);
                    }
                }.bind(this))
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});

	
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	// -------------------------------------------- GETTER FUNCTION STARTS---------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//

	Object.defineProperty (this, 'getTotalCompanies', {
        value : function (callback) {
            
            if(this.contractInstance) {
				this.contractInstance.totalCompanies (function(err, totalCompanies) {
					if(!err) {
						callback(null, Number(totalCompanies));
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});


	
	Object.defineProperty (this, 'getCompanyInfo', {
        value : function (_companyId, callback) {
            
            if(this.contractInstance) {
				this.contractInstance.getCompanyInfo (_companyId, function(err, result) {
					if(!err) {
						callback(null, result);
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});


	Object.defineProperty (this, 'getSchemeInfo', {
        value : function (_companyId, _schemeId, callback) {
            
            if(this.contractInstance) {
				this.contractInstance.getSchemeInfo (_companyId, _schemeId, function(err, result) {
					if(!err) {
						callback(null, result);
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});

	Object.defineProperty (this, 'getPieChartData', {
        value : function (_companyId, callback) {
            
            if(this.contractInstance) {
				this.contractInstance.getPieChartData (_companyId, function(err, result) {
					if(!err) {
						callback(null, result);
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});

	Object.defineProperty (this, 'getSchemeCount', {
        value : function (_companyId, callback) {
            
            if(this.contractInstance) {
				this.contractInstance.getSchemeCount (_companyId, function(err, result) {
					if(!err) {
						callback(null, result);
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});


	

	Object.defineProperty (this, 'getFarmerCountInScheme', {
        value : function (_companyId, _schemeId, callback) {
            
            if(this.contractInstance) {
				this.contractInstance.getRegFarmerCountInScheme (_companyId, _schemeId, function(err, result) {
					if(!err) {
						callback(null, result);
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});

	Object.defineProperty (this, 'getFarmerInfo', {
        value : function (_companyId, _farmerId, callback) {
            console.log("companyId : ", _companyId, "  farmerId : ", _farmerId);
            if(this.contractInstance) {
				this.contractInstance.getFarmerInfo (_companyId, _farmerId, function(err, result) {
					if(!err) {
						callback(null, result);
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});

	Object.defineProperty (this, 'getWind', {
        value : function (_companyId, _farmerId, _index, callback) {
            console.log("companyId : ", _companyId, "  farmerId : ", _farmerId, _index);
            if(this.contractInstance) {
				this.contractInstance.getWind (_companyId, _farmerId, _index, function(err, result) {
					if(!err) {
						callback(null, result);
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});

	Object.defineProperty (this, 'getTemperature', {
        value : function (_companyId, _farmerId, _index, callback) {
            if(this.contractInstance) {
				this.contractInstance.getTemperature (_companyId, _farmerId, _index, function(err, result) {
					if(!err) {
						callback(null, result);
					} else {
						callback(err, null);
					}
				}.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
	});








	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	// -------------------------------------------- UTILITY FUNCTION STARTS---------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------------------//

    Object.defineProperty (this, 'getBalanceInLink', {
        value : function (callback) {

            if(this.tokenInstance) {
                this.tokenInstance.balanceOf(this.loggedInAccount, function(err, balance) {
                    if(!err && balance) {
                        callback(null, balance.toNumber() / Math.pow(10, 18));
                    } else {
                        callback(err, 0);
                    }
                }.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }
            
            
        },
        enumerable: true,
        configurable: true
    });


    Object.defineProperty (this, 'sendLinkUsingMetamask', {
        value : function (value, callback) {
            if(this.tokenInstance) {
                
                let valuePrecision = value * Math.pow(10, 18);
                this.tokenInstance.transfer (CROPS_INSURANCE_CONTRACT_ADDR, valuePrecision, 
                    {
                        from: this.loggedInAccount,
                    },
                    function (err, txHash) {
                        if (txHash !== 'undefined') {
                            callback(null, txHash);
                        } else {
                            console.log('Error : ' + err);
                            callback(err, null);
                        }
                    }.bind(this)
                );

            } else {
                callback(new Error("Web3 is not initialized."), null);
            }
            
        },
        enumerable: true,
        configurable: true

    });

    Object.defineProperty (this, 'getTxConfirmation', {
        value : function (txHash, callback) {

            if(this.web3) {
                this.web3.eth.getTransactionReceipt(txHash, function (err, receipt) {
                    if (err) {
                        callback(err, null);
                    }
                    if (receipt !== undefined && receipt !== null) {
                      console.log("Receipt : ", receipt);
                      callback(null, receipt.status);
                    } else {
                      window.setTimeout(function () {
                        this.getTxConfirmation(txHash, callback);
                      }.bind(this), 1000);
                    }
                  }.bind(this));

            } else {
                callback(new Error("Web3 is not initialized."), null);
            }
        },
        enumerable: true,
        configurable: true
    });


    



    

}
