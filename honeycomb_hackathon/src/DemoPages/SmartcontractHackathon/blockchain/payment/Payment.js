import Web3 from "web3";

//Constant
const CHAIN_LINK_ABI = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"},{"name":"_data","type":"bytes"}],"name":"transferAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_subtractedValue","type":"uint256"}],"name":"decreaseApproval","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_addedValue","type":"uint256"}],"name":"increaseApproval","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"data","type":"bytes"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"}];
const CROPS_INSURANCE_CONTRACT_ABI = [{"constant":false,"inputs":[{"name":"_companyName","type":"string"},{"name":"_licenseNo","type":"string"},{"name":"_companyAddr","type":"string"},{"name":"_des","type":"string"},{"name":"_depositedAmount","type":"uint256"}],"name":"registerCompany","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_companyAddr","type":"address"},{"name":"_schemeId","type":"uint256"}],"name":"getSchemeInfo","outputs":[{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"string"},{"name":"","type":"uint256"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"totalCompanies","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_companyAddr","type":"address"}],"name":"getCompanyInfo","outputs":[{"name":"","type":"address"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_companyAddr","type":"address"},{"name":"_schemeId","type":"uint256"}],"name":"getFarmerCount","outputs":[{"name":"","type":"address"},{"name":"","type":"uint256"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_tLevel","type":"uint256[4]"},{"name":"_indemnityPercent","type":"uint256"},{"name":"_bearPercent","type":"uint256"},{"name":"_cropsName","type":"string"},{"name":"_cropsType","type":"uint256"},{"name":"_locationName","type":"string"}],"name":"addScheme","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"renounceOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"withdrawLink","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_companyAddr","type":"address"},{"name":"_schemeId","type":"uint256"}],"name":"getFactorInfo","outputs":[{"name":"","type":"address"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_farmerAddr","type":"string"},{"name":"_farmerName","type":"string"},{"name":"_landLocation","type":"string"},{"name":"_companyAddr","type":"address"},{"name":"_schemeId","type":"uint256"}],"name":"registerInScheme","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_companyAddr","type":"address"}],"name":"getSchemeCount","outputs":[{"name":"","type":"address"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_companyAddr","type":"address"},{"name":"_schemeId","type":"uint256"},{"name":"_farmerId","type":"uint256"}],"name":"getRegFarmerInfo","outputs":[{"name":"","type":"uint256"},{"name":"","type":"address"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"requestId","type":"bytes32"},{"indexed":true,"name":"price","type":"uint256"}],"name":"RequestEthereumPriceFulfilled","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"previousOwner","type":"address"}],"name":"OwnershipRenounced","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"previousOwner","type":"address"},{"indexed":true,"name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkRequested","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkFulfilled","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkCancelled","type":"event"}];
const CHAIN_LINK_ADDR = "0x20fE562d797A42Dcb3399062AE9546cd06f63280";
const CROPS_INSURANCE_CONTRACT_ADDR = "0x50c52288f70203e6a50d3af2c38cd523c4b7c016";

export default function PaymentUtil () {

    this.web3 = null;
    this.loggedInAccount = null;
    this.token = null;
    this.tokenInstance = null;
    this.contract = null;
    this.contractInstance = null;

    
    Object.defineProperty (this, 'initMetamask', {
        value : async function (callback) {
            if (typeof window.ethereum !== 'undefined') {
                this.web3 = new Web3(ethereum);
                try {
                    await ethereum.enable();
                    this.loggedInAccount = this.web3.eth.accounts[0];
                    this.token = this.web3.eth.contract(CHAIN_LINK_ABI);
                    this.tokenInstance = this.token.at(CHAIN_LINK_ADDR);

                    this.contract = this.web3.eth.contract(CROPS_INSURANCE_CONTRACT_ABI);
                    this.contractInstance = this.contract.at(CROPS_INSURANCE_CONTRACT_ADDR);
                    callback(null, true);
                    
                } catch (error) {
                    console.log("User denied account access.", error);
                    callback(error, null);

                }
            } else if (window.web3 !== null) {
                this.web3 = new Web3(web3.currentProvider);
                this.loggedInAccount = this.web3.eth.accounts[0];
                this.token = this.web3.eth.contract(CHAIN_LINK_ABI);
                this.tokenInstance = this.token.at(CHAIN_LINK_ADDR);

                this.contract = this.web3.eth.contract(CROPS_INSURANCE_CONTRACT_ABI);
                this.contractInstance = this.contract.at(CROPS_INSURANCE_CONTRACT_ADDR);
                callback(null, true);

            } else {
                callback(new Error("Metamask is not installed in browser. Please add it first in your browser"), null);
                
            }
        },
        enumerable: true,
        configurable: true
    });



    Object.defineProperty (this, 'initWeb3', {
        value : function (callback) {

            this.web3 = new Web3(new Web3.providers.HttpProvider(kovan_test_net));
            if(this.web3) {
                this.loggedInAccount = this.web3.eth.accounts[0];
                this.token = this.web3.eth.contract(CHAIN_LINK_ABI);
                this.tokenInstance = this.token.at(CHAIN_LINK_ADDR);

                this.contract = this.web3.eth.contract(CROPS_INSURANCE_CONTRACT_ABI);
                this.contractInstance = this.contract.at(CROPS_INSURANCE_CONTRACT_ADDR);
                callback(null, true);
            } else {
                callback(new Error("Web3js is failed to connect with network"), null);
            }
        },
        enumerable: true,
        configurable: true
    });


    Object.defineProperty (this, 'getBalanceInLink', {
        value : function (callback) {

            if(this.tokenInstance) {
                this.tokenInstance.balanceOf(this.loggedInAccount, function(err, balance) {
                    if(!err && balance) {
                        callback(null, {balance : balance.toNumber() / Math.pow(10, 18), address : this.loggedInAccount});
                    } else {
                        callback(err, {balance : 0, address : this.loggedInAccount});
                    }
                }.bind(this));
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }
            
            
        },
        enumerable: true,
        configurable: true
    });

    Object.defineProperty (this, 'sendLinkUsingMetamask', {
        value : function (value, callback) {
            if(this.tokenInstance) {
                
                let valuePrecision = value * Math.pow(10, 18);
                this.tokenInstance.transfer (CROPS_INSURANCE_CONTRACT_ADDR, valuePrecision, 
                    {
                        from: this.loggedInAccount,
                    },
                    function (err, txHash) {
                        if (txHash !== 'undefined') {
                            callback(null, txHash);
                        } else {
                            console.log('Error : ' + err);
                            callback(err, null);
                        }
                    }.bind(this)
                );

            } else {
                callback(new Error("Web3 is not initialized."), null);
            }
            
        },
        enumerable: true,
        configurable: true

    });

    Object.defineProperty (this, 'getTxConfirmation', {
        value : function (txHash, callback) {

            if(this.web3) {
                this.web3.eth.getTransactionReceipt(txHash, function (err, receipt) {
                    if (err) {
                        callback(err, null);
                    }
                    if (receipt !== undefined && receipt !== null) {
                      console.log("Receipt : ", receipt);
                      callback(null, receipt.status);
                    } else {
                      window.setTimeout(function () {
                        this.getTxConfirmation(txHash, callback);
                      }.bind(this), 1000);
                    }
                  }.bind(this));

            } else {
                callback(new Error("Web3 is not initialized."), null);
            }
        },
        enumerable: true,
        configurable: true
    });


    Object.defineProperty (this, 'registerCompany', {
        value : function (_companyName, _licenseNo, _companyAddr, _des, _depositedAmount, callback) {
            
            if(this.contractInstance) {
                console.log("Going register a company : ", _companyName, _licenseNo, _companyAddr, _des, _depositedAmount);
                this.contractInstance.registerCompany (_companyName, _licenseNo, _companyAddr, _des, _depositedAmount, 
                {
                    from : this.loggedInAccount,
                    gas:  "6000000",
                },
                function (err, txHash) {
                    if (txHash !== 'undefined') {
                        callback(null, txHash);
                    } else {
                        console.log('Error : ' + err);
                        callback(err, null);
                    }
                }.bind(this))
            } else {
                callback(new Error("Web3 is not initialized."), null);
            }

        },
        enumerable: true,
        configurable: true
    });



    

}
