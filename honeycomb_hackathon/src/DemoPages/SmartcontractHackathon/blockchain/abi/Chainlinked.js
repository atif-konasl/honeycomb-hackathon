var Chainlinked = { 
 abi: function(){ 
 return [{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkRequested","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkFulfilled","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkCancelled","type":"event"}] 
}, 
 data:function() { 
 return "0x60806040526001600455348015601457600080fd5b5060358060226000396000f3006080604052600080fd00a165627a7a72305820c4f6249b00a6497ec1e33753016f6c88411b8ce00d9926e881f101cf4b7cf3db0029"  
}, 
}

export default Chainlinked;
