var ChainlinkClient = { 
 abi: function(){ 
 return [{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkRequested","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkFulfilled","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"id","type":"bytes32"}],"name":"ChainlinkCancelled","type":"event"}] 
}, 
 data:function() { 
 return "0x60806040526001600455348015601457600080fd5b5060358060226000396000f3006080604052600080fd00a165627a7a723058206a15e3ec660ae6295465e7884bc3ecc51f98c1dd469898aa06c4dde277a6b64d0029"  
}, 
}

export default ChainlinkClient;
