#Author Md. Rayhanul Masud
#Date   3 June, 2019
# This file contains code to compile solidity code and parse the solidity code to find the public primitive variables

#!/bin/bash

contractSourcePath=$1					# Path of the Solidity Code
outputDirPath=$2					# Output Directory of the compiler generated outputs
solcCompilerExePath=$3					# Path of Solc Compiler


if [[ -f "$solcCompilerExePath" ]]; then
	consoleOutput=$($solcCompilerExePath $contractSourcePath  -o $outputDirPath  --overwrite --bin --abi --hashes)
else
	echo "Compiler is not found in the provided path. Please check"
fi

#if command -v python &>/dev/null; then
#	python ./parser.py $contractSourcePath
#else
#	echo "Python is not installed. Cannot Parse public variables"
#fi
