import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';
import { Link } from "react-router-dom";
import LoaderGif from "../../../assets/insurance/loader.gif";
import SuccessIcon from "../../../assets/insurance/successIcon.png";


export default class CompanyRegistrationForm extends React.Component {
    constructor(props) {
        super(props);
        console.log("In insurance company registration form..., contractInstance : ", this.props.contractInstance);
        this.state = {
            isConfirm : false,
            status : false,
        }
        this.props.contractInstance.getTxConfirmation(this.props.txHash, function(err, status) {
            if(!err) {
                console.log("Status : ", status);
                if(status == '0x1') {
                    this.setState({
                        isConfirm : true,
                        status : true,
                    });
                } else {
                    this.setState({
                        isConfirm : true,
                        status : false,
                    });
                }
            }
        }.bind(this));
    }

    componentDidMount() {
        
    }

    



    render() {
        return (
            <div style={{textAlign:"center", fontFamily:"'Yantramanav', sans-serif", lineHeight:"1.5"}}>
                <Row className="text-center">
                    <Col md="2"></Col>
                    <Col md="8">
                        <Card className="main-card mb-3">
                            <CardBody>
                                {
                                    this.state.isConfirm ? 
                                        this.state.status ?
                                            <div>
                                                <img width={40} className="rounded-circle" src={SuccessIcon} alt="Avatar" /><br/>
                                                <p>Your transaction is succssfull.<br/>
                                                    Transaction Hash : {this.props.txHash}
                                                </p>
                                            </div>

                                        :
                                            <div>
                                                <img width={40} className="rounded-circle" src={SuccessIcon} alt="Avatar" /><br/>
                                                <p>Your transaction is failed.<br/>
                                                    Transaction Hash : {this.props.txHash}
                                                </p>
                                            </div>
                                    :
                                        <div>
                                            <img width={40} className="rounded-circle" src={LoaderGif} alt="Avatar" /><br/>
                                            <p>Your transaction is being processed.<br/>
                                                Transaction Hash : {this.props.txHash}
                                            </p>
                                        </div>
                                }

                                
                                    <Link to={{
                                            pathname: "/hackathon/home",
                                        }}>
                                            <button type="button" className="btn btn-primary" style={{fontSize:"16px", width:"100%"}}><strong>Back</strong></button>
                                    </Link>       
                            </CardBody>
                        </Card>
                    </Col>
                    <Col md="2"></Col>
                </Row>
            </div>   
                                                     
        )
    }
}