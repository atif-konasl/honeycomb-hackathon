import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';


export default class MetaMaskInstallStatus extends React.Component {
    constructor(props) {
        super(props);
        console.log("In metamask install status : ", );
        this.state = {
            
        }
    }

    componentDidMount() {
        
    }

    
    render() {
        return (
            <div style={{textAlign:"center", fontFamily:"'Yantramanav', sans-serif", lineHeight:"1.5"}}>
                <br/><br/>
                <Row className="text-center">
                    <Col md="2"></Col>
                    <Col md="8">
                        <Card className="main-card mb-3" style={{height:"300px", paddingTop:"100px", color:"black"}}>
                            <CardBody>
                                <h5><strong>Please Install  <a href="https://metamask.io/"> MetaMask </a> In Your Browser</strong></h5>
                                <p style={{color:"#6c757d"}}>For interact with this project, you need to install <a href="https://metamask.io/"> MetaMask </a> with your browser</p>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col md="2"></Col>
                </Row>
            </div>   
                                                     
        )
    }
}