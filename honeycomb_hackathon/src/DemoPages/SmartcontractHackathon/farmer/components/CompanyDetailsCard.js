import React from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';
import { Link } from "react-router-dom";

import SchemeDetails from "./SchemeDetails";

export default class CompanyDetailsCard extends React.Component {
    constructor(props) {
        super(props);
        console.log("In scheme details card...", this.props.companyInfo);
        this.state = {
        }
    }

    componentDidMount() {

    }



    render() {
        return (
            <div style={{textAlign:"center", padding:"30px"}}>
                <Card className="main-card mb-3 widget-chart">
                    <CardBody>
                        <CardTitle>
                            <strong>{this.props.companyInfo.name}</strong>
                        </CardTitle>
                        <Form style={{fontSize:"12px"}}>
                            <FormGroup>
                                <Label style={{color:"#6c757d"}}>
                                    {this.props.companyInfo.description}
                                </Label><br/>
                                <Label>Address : {this.props.companyInfo.address}</Label><br/>
                                <Label>Total Capital : {this.props.companyInfo.totalCapital / Math.pow(10,18)} LINK</Label><br/>
                                <Label>Total Insurance Policies : {this.props.companyInfo.totalSchemeCount}</Label><br/>
                                <Label>Total Registerd Farmers : {this.props.companyInfo.totalRegFarmersCount}</Label><br/>
                            </FormGroup>

                              

                            <Link to={{
                                    pathname: "/hackathon/show-scheme-page",
                                    state: {
                                        contractInstance: this.props.contractInstance,
                                        companyId : this.props.companyInfo.companyId,
                                        companyName : this.props.companyInfo.name,
                                    }
                                }}>
                                <button type="button" className="btn btn-primary" style={{fontSize:"12px", width:"100%"}}><strong>View Insurance Policies</strong></button>
                
                            </Link>
                            
                                    
                        </Form>
                    </CardBody>
                </Card>
            </div>                                             
        );      
    }
}