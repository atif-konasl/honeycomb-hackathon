import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle
} from 'reactstrap';


import Contract from "../..//blockchain/InsuranceContract";
import CompanyDetails from "./CompnayDetails";
import Registration from "./RegistrationScheme";
import ClaimPage from "./ClaimPage";

//Background Image
import bg1 from "../../../../assets/insurance/homepage_bg.jpeg";




export default class FarmerMainPage extends React.Component {
    constructor(props) {
        super(props);
        console.log("In farmer page...");
        this.state = {
            selectedOption : "",
            accountAddr : "",
            contractInstance : null,
        }
        this.handleSelectionBtn = this.handleSelectionBtn.bind(this);
        this.contractInstance = new Contract();
        this.contractInstance.initMetamask(function(err, result) {
            if(!err) { 
                console.log("Metamask is connected successfully. Account address : ", this.contractInstance.loggedInAccount);
                this.setState({
                    accountAddr : this.contractInstance.accountAddr,
                    contractInstance : this.contractInstance,
                });
            } else {
                console.log(err);
            }
        }.bind(this));
    }

    componentDidMount() {
        
    }

    handleSelectionBtn(event) {
        console.log("Value is selected : ", event.target.value);
        this.setState({
            selectedOption : event.target.value,
        });
    }


    render() {
        return (
            <div style={{margin:"0px", textAlign:"center", backgroundImage:`url(${bg1})`, backgroundSize:"cover", backgroundPosition:"center", width:"100%", height:"50vh", zIndex:"-100"}}>
            <h4 style={{marginLeft:"15px", marginTop:"15px", float:"left", color:"white", fontFamily:"'Ubuntu', sans-serif"}}><strong>Oracle Crops Insurance</strong></h4>

                <div style={{ marginTop:"15px", fontFamily:"'Ubuntu', sans-serif", color:"black"}}>
                    <div style={{ fontFamily:"'Yantramanav', sans-serif",marginTop : "20px", marginLeft:"200px", marginRight:"200px", lineHeight:"1.5"}}>
                        <p style={{fontSize:"40px"}}> Farmer Corner</p>
                    </div>
                    <div style={{fontFamily:"'Yantramanav', sans-serif"}}>
                        <Row className="text-center">
                            <Col md="2"></Col>

                            <Col md="4">
                                <Card className="main-card mb-3" style={{backgroundColor:"transparent"}}>
                                    <CardBody>
                                        <button value="browse_policy" onClick= {(e) => this.handleSelectionBtn(e)} type="button" className="btn btn-primary" style={{ fontSize:"25px", border:"none", width:"250px", height:"220px", float:"left", color:"black", opacity:"0.8", background:"#eaede6"}}><strong>Insurance Policies</strong></button>
                                    </CardBody>   
                                </Card>
                            </Col>

                            <Col md="4">
                                <Card className="main-card mb-3" style={{backgroundColor:"transparent"}}>
                                    <CardBody>
                                        <button value="track_claim" onClick= {(e) => this.handleSelectionBtn(e)} type="button" className="btn btn-primary" style={{fontSize:"25px", border:"none", width:"250px", height:"220px", float:"right", color:"black", opacity:"0.8", background:"#eaede6"}}><strong>Request or Track Claim</strong></button>
                                    </CardBody>   
                                </Card>
                            </Col>

                            <Col md="2"></Col>
                        </Row>
                    </div>
                </div>

                <div style={{ fontFamily:"'Ubuntu', sans-serif", marginTop:"50px", color:"black", lineHeight:"1.5", textAlign:"center", backgroundColor:"#e2e3e1"}}>   
                    {
                        this.state.selectedOption == "browse_policy" ? 
                            <CompanyDetails contractInstance={this.state.contractInstance} />
                            :
                            null
                    }

                    {
                        this.state.selectedOption == "track_claim" ?
                            <ClaimPage contractInstance={this.state.contractInstance} />
                            :
                            null
                    }

                    {
                        this.state.selectedOption == "" ? 
                            <Registration contractInstance={this.props.location.state.contractInstance} scheme={this.props.location.state.scheme}/>
                            :
                            null
                    }
                </div>
                

                <div style={{ fontFamily:"'Ubuntu', sans-serif",marginLeft:"200px", marginRight:"200px", marginTop:"100px", color:"black", lineHeight:"1.5", textAlign:"center"}}>   
                    <h4><strong>How it Works?</strong></h4>
                    <p style={{fontSize:"14px", color:"#6c757d"}}>
                        This solution works on weather data like temperature, wind pressure, rainfall, drought, etc.
                        Our smart contract fetch data from <a href="https://www.worldweatheronline.com/"> World Weather Online </a>
                        and calculate loss from these data. Farmer can easily get his/her claim amount through smart contract.
                        You need metamask wallet for interaction.
                    </p>
                    <br/><br/>
                    <div style={{fontFamily:"'Yantramanav', sans-serif"}}>
                        <Row className="text-center">
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Total Farmers
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            50
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Total Companies<br/>
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            5
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="4">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <CardTitle>
                                            Area Coverage<br/>
                                        </CardTitle>
                                        <div className="widget-numbers">
                                            30
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>

            </div>   
                                                     
        )
    }
}