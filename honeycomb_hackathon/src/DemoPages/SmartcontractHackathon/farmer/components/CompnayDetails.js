import React from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';


import CompanyDetailsCard from "./CompanyDetailsCard";

export default class CompanyDetails extends React.Component {
    constructor(props) {
        super(props);
        console.log("In company details page...");
        this.state = {
            companyDetails : [],
        }
        this.showCompanyDetailsCards = this.showCompanyDetailsCards.bind(this);
    }

    componentDidMount() {
        this.props.contractInstance.getTotalCompanies(function(err, totalCompany) {
            if(!err) {
                console.log("Total company : ", totalCompany);

                for(let i = 0; i < totalCompany; i++) {
                    this.props.contractInstance.getCompanyInfo(i, function(err, result) {
                        if(!err) {
                            console.log("Company details : ", result);

                            let companyDetailsList = this.state.companyDetails;

                            let obj = {
                                companyId : i,
                                name : result[0],
                                description : result[1],
                                address : result[2],
                                totalCapital : Number(result[3]),
                                totalSchemeCount : Number(result[4]),
                                totalRegFarmersCount : Number(result[5]),
                            }

                            companyDetailsList.push(obj);
                            
                            this.setState({
                                companyDetails : companyDetailsList,
                            });

                            console.log("Companies in state : ", this.state.companyDetails);
                        }
                    }.bind(this));
                }
            }
        }.bind(this));
    }


    showCompanyDetailsCards() {
        const companyList = this.state.companyDetails.map((companyInfo, index) => {
            return (
                <Col md="4" key={index}>
                    <CompanyDetailsCard contractInstance={this.props.contractInstance} companyInfo = {companyInfo}/>
                </Col>
            );
        });
        return companyList;
    }


    render() {

        const companyList = this.showCompanyDetailsCards();
        return (
            <div style={{textAlign:"center", padding:"30px"}}>
                <h3><strong>Our Insurance Providers</strong></h3>
                <br/><br/>
                <Row className="text-center">
                   {companyList}
                </Row>

                <br/>
            </div>   
                                                     
        )
    }
}