
import React, { Component } from 'react';
import CanvasJSReact from "../../../../assets/canvasjs.react";

var CanvasJSChart = CanvasJSReact.CanvasJSChart;
 
class BarChart extends Component {

    constructor(props) {
        super(props);
        console.log("In barchart...", this.props.data);
        this.state = {
            dataPoints: [],
        }
        for(let i = 0; i < this.props.data.length; i++) {
            let date = "0"+ (i+1) +"-" + this.props.month.slice(0,3) + "-2019";
            let array = this.state.dataPoints;
            let obj = {
                label : date, 
                y : this.props.data[i]
            }
            console.log("Obj : ", obj);
            array.push(obj);
            this.setState({
                dataPoints : array,
            });
        }
        console.log("Data points : ", this.state.dataPoints);
    }

	render() {
        const options = {
            title: {
                text: this.props.text
            },
            data: [
                {
                    type: "column",
                    dataPoints : this.state.dataPoints
                }
            ]
        }
        return (
        <div>
            <CanvasJSChart options = {options}
                /* onRef={ref => this.chart = ref} */
            />
            {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
        </div>
        );
    }
}

export default BarChart;

