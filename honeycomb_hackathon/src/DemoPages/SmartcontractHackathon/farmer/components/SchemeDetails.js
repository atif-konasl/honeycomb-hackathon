import React from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';


import SchemeDetailsCard from "./SchemeDetailsCard";

const MonthMapping = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

export default class SchemeDetails extends React.Component {
    constructor(props) {
        super(props);
        console.log("In scheme details page...", this.props.companyId, this.props.contractInstance);
        this.state = {
            companyId : null,
            schemeDetails : [],
        }
        this.showSchemeCards = this.showSchemeCards.bind(this);
    }

    componentDidMount() {
        console.log("Company id : ", this.props.companyId);
        this.props.contractInstance.getSchemeCount(this.props.companyId, function(err, totalScheme) {
            if(!err) {
                let total = Number(totalScheme);
                console.log("Total scheme : ", total);
                for(let i = 0; i < total; i++) {
                    this.props.contractInstance.getSchemeInfo(this.props.companyId, i, function(err, result) {
                        if(!err) {
                            console.log("Scheme details : ", result);
                            let schemeDetailList = this.state.schemeDetails;
                            
                            let monthValue = Number(result[5].split("-")[1]);
                            console.log("Month value : ", monthValue);

                            let obj = {
                                companyId : this.props.companyId,
                                schemeId :  Number(result[0]),
                                schemeName : result[1],
                                cropsName : result[2],
                                location : result[3],
                                schemePrice : Number(result[4]),
                                month : MonthMapping[monthValue - 1],
                            }

                            schemeDetailList.push(obj);
                            
                            this.setState({
                                schemeDetails : schemeDetailList,
                            });

                            console.log("Scheme in state : ", this.state.schemeDetails);
                        }
                    }.bind(this));
                }
            }
        }.bind(this));
    }


    showSchemeCards() {
        const schemeList = this.state.schemeDetails.map((scheme, index) => {
            return (
                <Col md="4" key={index}>
                    <SchemeDetailsCard  contractInstance = {this.props.contractInstance} scheme = {scheme}/>
                </Col>
            );
        });
        return schemeList;
    }


    render() {
        const schemeList = this.showSchemeCards();
        return (
            <div style={{textAlign:"center", padding:"30px"}}>
                <h3><strong>{this.props.companyName}</strong></h3>
                <h5><strong>Insurance Policies</strong></h5>
                
                <Row className="text-center">
                    {schemeList}
                </Row>

                <br/>
            </div>   
                                                     
        )
    }
}