import React, {Component, Fragment} from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';

import TxStatus from "../../commonComponents/StatusCrad";


export default class RegistrationScheme extends React.Component {
    constructor(props) {
        super(props);
        console.log("In scheme registration form...", this.props.scheme);
        this.state = {
            name:"",
            address:"",
            hasSufficientBalance:false,
            isShowingStatus : false,
            txHash : "",
            depositedAmount : 0,
        }
        this.handleRegisterBtn = this.handleRegisterBtn.bind(this);
        this.updateName = this.updateName.bind(this);
        this.updateAddr = this.updateAddr.bind(this);

        this.props.contractInstance.getBalanceInLink(function(err, balance) {
            if(!err) {
                this.setState({
                    depositedAmount : balance,
                });
                if(balance > this.props.scheme.schemePrice) {
                    this.setState({
                        hasSufficientBalance : true,
                    });
                }
            }
        }.bind(this));

    }

    componentDidMount() {

    }

    updateName(event) {
        this.setState({
            name : event.target.value,
        });
    }

    updateAddr(event) {
        this.setState({
            address : event.target.value,
        });
    }

    handleRegisterBtn(event) {
        console.log("Register a scheme with these values : ", this.state.name, 
                this.state.address, this.props.scheme.companyId, this.props.scheme.schemeId, this.props.scheme.schemePrice);

        event.preventDefault();

        this.props.contractInstance.registerInScheme (
            this.state.name, this.state.address, this.props.scheme.companyId,
            this.props.scheme.schemeId, this.props.scheme.schemePrice,
             function(err, txHash) {
                if(!err && txHash) {
                    console.log("Transaction hash : ", txHash);
                        this.setState({
                            isShowingStatus : true,
                            txHash : txHash,
                        });
                } else {
                    console.log("Error : ", err);
                }
            }.bind(this));

    }



    render() {
        return (
            <div style={{textAlign:"center", padding:"30px", marginLeft:"200px", marginRight:"200px"}}>

                { this.state.isShowingStatus ? 
                    <h4><strong>Transaction Status</strong></h4> 
                    : 
                    <h4><strong>Registration <br/>in <br/>{this.props.scheme.schemeName}</strong></h4>
                }
                
                <br/><br/>
                {
                    this.state.isShowingStatus ? 
                        <TxStatus contractInstance={this.props.contractInstance} txHash = {this.state.txHash}/>
                        :
                        <Row className="text-center">
                            <Col md="3"></Col>
                            <Col md="6">
                                <Card className="main-card mb-3 widget-chart">
                                    <CardBody>
                                        <Form style={{fontSize:"12px"}}>
                                            <FormGroup>
                                                <Label>Your Blockchain Address : {this.props.contractInstance.loggedInAccount}</Label>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Enter Your Name : </Label>
                                                <Input style={{fontSize:"12px"}} type="text" placeholder="Enter your name"
                                                    onChange = {this.updateName} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label>Enter Your Address: </Label>
                                                <Input style={{fontSize:"12px"}} type="text" placeholder="Enter address"
                                                    onChange = {this.updateAddr} />
                                            </FormGroup>

                                            <FormGroup>
                                                <Label>Scheme Price (In LINK): {this.props.scheme.schemePrice} LINK</Label>
                                            </FormGroup>

                                            <FormGroup>
                                                <Label>Your Deposited Toke (in LINK) : {this.state.depositedAmount} LINK</Label>
                                            </FormGroup>

                                            { 
                                                this.state.hasSufficientBalance
                                                ?
                                                <button onClick={this.handleRegisterBtn} type="button" className="btn btn-primary"
                                                     style={{width:"100%"}}>Submit</button>
                                                :
                                                <button disabled type="button" className="btn btn-primary" style={{width:"100%"}}>In-Sufficient-Fund</button>
                                        
                                            }
                                            </Form>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md="3"></Col>
                        </Row>
                    }
            </div>   
                                                     
        );
    }
}