import React from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';

import RegistrationScheme from "./RegistrationScheme"
import { Link } from "react-router-dom";

export default class SchemeDetailsCard extends React.Component {
    constructor(props) {
        super(props);
        console.log("In scheme details card...", this.props.scheme);
        this.state = {
            isShowingSchemeReg : false,
            farmerCount : 0,
        }
        this.handleBtn = this.handleBtn.bind(this);
    }

    componentDidMount() {
        this.props.contractInstance.getFarmerCountInScheme(this.props.scheme.companyId, this.props.scheme.schemeId, 
            function(err, result) 
        {
            if(!err) {
                console.log("Registered farmers in this scheme : ", Number(result[1]));
                this.setState({
                    farmerCount : Number(result[1]),
                });
            }
        }.bind(this));
    }

    handleBtn() {
        console.log("Clicked on insurance btn ");
        this.setState({
            isShowingSchemeReg : true,
        });
    }

    render() {

            return (
                <div style={{textAlign:"center", padding:"30px"}}>
                    <br/>
    
                    <Card className="main-card mb-3 widget-chart">
                        <CardBody>
                            <CardTitle>
                                {this.props.scheme.schemeName}
                            </CardTitle>
                            <Form style={{fontSize:"12px"}}>
                                <FormGroup>
                                    <Label>Crops Name : {this.props.scheme.cropsName}</Label><br/>
                                    <Label>Scheme Price : {this.props.scheme.schemePrice} LINK</Label><br/>
                                    <Label>Location : {this.props.scheme.location}</Label><br/>
                                    <Label>Month : {this.props.scheme.month}</Label><br/>
                                    <Label>Status : Active</Label><br/>
                                    <Label>Total Registerd Farmers : {this.state.farmerCount}</Label><br/>
                                </FormGroup>
                                
                            </Form>

                            <Link to={{
                                    pathname: "/hackathon/buy-insurance-policy",
                                    state: {
                                        contractInstance: this.props.contractInstance,
                                        scheme : this.props.scheme,
                                    }
                                }}>
                                <button type="button" className="btn btn-primary" style={{fontSize:"12px", width:"100%"}}><strong>Register in Policy</strong></button>
                
                            </Link>
                           
                        </CardBody>
                    </Card>
                </div>   
                                                         
            );
        
    }
}