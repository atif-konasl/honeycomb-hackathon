import React from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';

import ClaimDetailsCard from "./ClaimDetailsCard";

const CROPS_INSURANCE_CONTRACT_ADDR = "0x28d93e988f703103e0b8c2c721d001055ba282ce";
export default class ClaimPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idArray : [],
        }
        this.showClaimCards = this.showClaimCards.bind(this);
    }

    componentDidMount() {

        let loggedInAddr = "0x" +  "000000000000000000000000" + this.props.contractInstance.loggedInAccount.slice(2);
        let options = {
            fromBlock: 0,
            toBlock: 'latest',
            topics:["0x2e5ffabcfc0de229779a6584a8621f1a4b6bb65c41fe53e3c3e466c042d0a6a0",loggedInAddr, null], 
        };

        let filter =  this.props.contractInstance.web3.eth.filter(options);
        filter.get(function(error, result) {
            if(!error) {
                console.log("Result : ", result);
                for(let i = result.length - 1; i >= 0; i--) {
                    if(result[i].address == CROPS_INSURANCE_CONTRACT_ADDR) {
                        let idArrayList = this.state.idArray;
                        let obj = {
                            companyId : parseInt(result[i].topics[2]),
                            farmerId : parseInt(result[i].topics[3]),
                        }
                        idArrayList.push(obj);
                        this.setState({
                            idArray : idArrayList,
                        });
                        console.log("idArray in state : ", this.state.idArray);
                    }  
                }
            }
        }.bind(this));
    }

    

    showClaimCards() {
        
        const claimCardList = this.state.idArray.map((ids, index) => {
            return (
                <Col md="10" key={index}>
                    <ClaimDetailsCard contractInstance = {this.props.contractInstance}  idGroup = {ids}/>
                </Col>
                
            );
        });
        return claimCardList;
    }


    render() {
        const claimCards = this.showClaimCards();
        return (
            <div style={{textAlign:"center", padding:"30px"}}>
                <br/><br/>
                <Row className="text-center">
                    {claimCards}
                </Row>
            </div>   
                                                     
        )
    }
}