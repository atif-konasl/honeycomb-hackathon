import React from 'react';
import {
    Row, Col,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup, 
    Label, 
    Input,
} from 'reactstrap';

import TxStatus from "../../commonComponents/StatusCrad"
import BarChart from "./BarChart";

const MonthMapping = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const Status = ['NOT CLAIMED YET', 'CLAIM REQUESTED', 'CLAIM IN PROCESSING', 'READY TO REDEEM', 'CLAIM FINISHED']

export default class ClaimCardDetails extends React.Component {
    constructor(props) {
        super(props);
        console.log("In claim details card...", this.props.idGroup);
        this.state = {
            isShowingStatus : false,
            txHash : "",
            schemeId : "",

            windData : [],
            tempData : [],
            stdDeviationWinds : 0,
            stdDeviationTemps : 0,
            loss : 0,
            statusIndx : 0,

            schemeName : "",
            cropsName : "",
            location : "",
            schemePrice : "",
            month : "",

        }
        this.handleBtn = this.handleBtn.bind(this);
        this.getWindData = this.getWindData.bind(this);
        this.getTemperatureData = this.getTemperatureData.bind(this);
        this.claim = this.claim.bind(this);
        this.withdraw = this.withdraw.bind(this);
        this.getSchemeInfo = this.getSchemeInfo.bind(this);
    }

    componentDidMount() {
        this.props.contractInstance.getFarmerInfo(this.props.idGroup.companyId, this.props.idGroup.farmerId, 
            function(err, result) 
        {
            if(!err) {
                let stdDevWinds = Number(result[2]);
                let stdDevTemps = Number(result[3]);
                let loss = Number(result[4]) / Math.pow(10, 18);
                let stateIndx = Number(result[5]);
                let schemeId = Number(result[1]);

                console.log("schemeId : ", schemeId, " stdDevTemps : ", stdDevTemps, "  stdDevWinds : ",stdDevWinds, "  loss : ", loss, "  stateIndx ; ", stateIndx);
                this.setState({
                    stdDeviationWinds : stdDevWinds,
                    stdDeviationTemps : stdDevTemps,
                    loss : loss,
                    statusIndx : stateIndx,
                    schemeId: schemeId,
                });
                this.getSchemeInfo();
                if(stateIndx >= 3) {
                    this.getTemperatureData();
                    this.getWindData();
                }
            }
        }.bind(this));

        
        
       
        
    }

    getSchemeInfo() {
        this.props.contractInstance.getSchemeInfo(this.props.idGroup.companyId, this.state.schemeId, function(err, result) {
            if(!err) {
                console.log("Scheme details : ", result);
               
                
                let monthValue = Number(result[5].split("-")[1]);
                console.log("Month value : ", monthValue);

                this.setState({
                    schemeName : result[1],
                    cropsName : result[2],
                    location : result[3],
                    schemePrice : Number(result[4]),
                    month : MonthMapping[monthValue - 1],
                });
            }
        }.bind(this));
    }

    getTemperatureData() {
        for(let i = 0; i < 10; i++) {

            this.props.contractInstance.getTemperature(this.props.idGroup.companyId, 
                this.props.idGroup.farmerId, i, function(err, result) 
            {
                if(!err) {
                    console.log("Temp : ", Number(result[2]));
                    let tempDataList = this.state.tempData;
                    tempDataList.push(Number(result[2]));
                    this.setState({
                        tempData : tempDataList,
                    });
                }
            }.bind(this));
        }
    }

    getWindData() {

        for(let i = 0; i < 10; i++) {
            this.props.contractInstance.getWind(this.props.idGroup.companyId, 
                this.props.idGroup.farmerId, i, function(err, result) 
            {
                if(!err) {
                    console.log("Wind : ", Number(result[2]));
                    let windDataList = this.state.windData;
                    windDataList.push(Number(result[2]));
                    this.setState({
                        windData : windDataList,
                    });
                }
            }.bind(this));
        }

    }

    claim() {
        this.props.contractInstance.claimRequest(this.props.idGroup.companyId, this.state.schemeId,
            this.props.idGroup.farmerId, function(err, txHash) {
                if(!err && txHash) {
                    console.log("Transaction hash : ", txHash);
                        this.setState({
                            isShowingStatus : true,
                            txHash : txHash,
                        });
                } else {
                    console.log("Error : ", err);
                }
            }.bind(this))
    }

    withdraw() {
        this.props.contractInstance.withdrawClimedLink(this.props.idGroup.companyId, this.props.idGroup.farmerId, 
            function(err, txHash) {
                if(!err && txHash) {
                    console.log("Transaction hash : ", txHash);
                        this.setState({
                            isShowingStatus : true,
                            txHash : txHash,
                        });
                } else {
                    console.log("Error : ", err);
                }
            }.bind(this))
    }

    handleBtn() {
        console.log("Clicked on insurance btn ");
        this.setState({
            isShowingSchemeReg : true,
        });
    }

    render() {
            return (
                <div style={{textAlign:"center", marginLeft:"200px", padding:"30px"}}>
                    { this.state.isShowingStatus ? 
                        <h4><strong>Transaction Status</strong></h4> 
                        : 
                        <h4><strong>Claim Report of {this.state.schemeName}</strong></h4>
                    }
                    <br/><br/>
                    {
                        this.state.isShowingStatus ? 
                            <TxStatus contractInstance={this.props.contractInstance} txHash = {this.state.txHash}/>
                            :
                            <Card className="main-card mb-3 widget-chart">
                                <CardBody>
                                    
                                    <Form style={{fontSize:"12px"}}>
                                        <FormGroup>
                                            <Label>Crops Name : {this.state.cropsName}</Label><br/>
                                            <Label>Scheme Price : {this.state.schemePrice} LINK</Label><br/>
                                            <Label>Location : {this.state.location}</Label><br/>
                                            <Label>Month : {this.state.month}</Label><br/>
                                            <Label>Claim Status  :  
                                                <div className="badge badge-warning">{Status[this.state.statusIndx]}</div>
                                            </Label><br/>
                                        </FormGroup>
                                        {
                                                this.state.statusIndx > 2
                                                ?
                                                <FormGroup>
                                                    <Label> Standard Deviation of Wind Data : {this.state.stdDeviationWinds}</Label><br/>
                                                    <Label> Standard Deviation of Temperature Data : {this.state.stdDeviationTemps}</Label><br/>
                                                    <Label>Ready to Redeem Claim Amount : {this.state.loss} LINK</Label><br/>
                                                    <Label>Month : {this.state.month}</Label><br/>
                                                </FormGroup>
                                                :
                                                null
                                        }                     
                                    </Form>

                                    {
                                        this.state.statusIndx == 0
                                        ?
                                        <button onClick={this.claim} type="button" className="btn btn-primary" style={{fontSize:"12px", width:"100%"}}><strong>Claim</strong></button>
                                        :
                                        null
                                    }

                                    {
                                        this.state.statusIndx == 3
                                        ?
                                        <button onClick={this.withdraw} type="button" className="btn btn-primary" style={{fontSize:"12px", width:"100%"}}><strong>Withdraw Claim Amount</strong></button>
                                        :
                                        null
                                    }

                                    
                                <br/><br/>
                                {this.state.tempData.length == 10 ? <BarChart month={this.state.month} text={"Average Temperature Data (In " + this.state.month + ",2019, " + this.state.location + ")"} data={this.state.tempData}/> : null }
                                <br/><br/>
                                {this.state.windData.length == 10 ? <BarChart month={this.state.month} text={"Average Wind Data (In " + this.state.month + ",2019, " + this.state.location + ")"} data={this.state.windData}/> : null }
                                </CardBody>
                            </Card>
                        }
                </div>   
                                                         
            );
        
    }
}