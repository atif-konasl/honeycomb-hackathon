import React, {Fragment} from 'react';
import {Route} from 'react-router-dom';

// DASHBOARDS

import HomePage from "./homePage/HomePageContainer";
import InsuranceCompanyPage from "./insuranceCompany/InsuranceCompanyPage";
import TxStatus from "./commonComponents/StatusCrad";
import Farmer from "./farmer/FarmerMainPage";
import SchemeDetails from "./farmer/components/SchemeDetails";
import SchemeDetailsPage from "./farmer/components/SchemeDetailsPage";
import SchemeRegistration from "./farmer/components/RegistrationSchemePage";

// Layout
import AppFooter from '../../Layout/AppFooter/';




const Insurance = ({match}) => (
    <Fragment>
        <div className="app-main">
            <Route path={`${match.url}/home`} component={HomePage}/>
            <Route path={`${match.url}/farmer-corner`} component={Farmer}/>
            <Route path={`${match.url}/insurance-policies`} component={SchemeDetails}/>
            <Route path={`${match.url}/insurance-company`} component={InsuranceCompanyPage}/>
            <Route path={`${match.url}/tx-status`} component={TxStatus}/>
            <Route path={`${match.url}/show-scheme-page`} component={SchemeDetailsPage}/>
            <Route path={`${match.url}/buy-insurance-policy`} component={SchemeRegistration}/>
            
        </div>
        {/* <AppFooter/> */}
    </Fragment>
);

export default Insurance;