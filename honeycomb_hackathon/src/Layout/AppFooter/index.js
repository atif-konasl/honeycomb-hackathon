import React, {Fragment} from 'react';

class AppFooter extends React.Component {
    render() {


        return (
            <Fragment>
                <div className="app-footer" style={{marginBottom:"0px"}} >
                    <div className="app-footer__inner" style={{backgroundColor:"transparent"}}>

                    <div className="footer-copyright text-center py-3" style={{marginLeft:"40%"}}>© 2018 Copyright -
                        <a href="https://mdbootstrap.com/education/bootstrap/"><strong> True Betting Team</strong> </a>
                    </div>
    
                    </div>
                </div>
            </Fragment>
        )}
}

export default AppFooter;