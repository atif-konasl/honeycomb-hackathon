import React, {Component} from 'react';
import TitleComponent2 from './PageTitleExamples/Variation2'

class PageTitle extends Component {

    render() {
        return (

            <div className="app-page-title" style={{padding:"5px", backgroundColor:"transparent", borderBottom: ".5px solid gray"}}>
                <div className="page-title-wrapper">
                    <h6 style={{color:"black"}}><strong>FOOTBALL (6)</strong></h6>
                </div>
            </div>
        );
    }
}

export default PageTitle;