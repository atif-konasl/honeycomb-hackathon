import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default class DropdownHoverNavbar extends React.Component {
  constructor(props) {
    super(props);

    console.log("header : ", this.props.headerName,  "   options : ", this.props.options);
    this.toggle = this.toggle.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
    this.generateOptions = this.generateOptions.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  onMouseEnter() {
    this.setState({dropdownOpen: true});
  }

  onMouseLeave() {
    this.setState({dropdownOpen: false});
  }

  generateOptions() {
      const optionList = this.props.options.map((item, index) => {
          return(
            <DropdownItem key={index}>
                <a className="nav-link" href={this.props.hrefs[index]}>{item}</a>
            </DropdownItem>
          )
      });
      return optionList;
  }

  render() {
    const optionList = this.generateOptions();
    return (
      <Dropdown style={{marginRight:"15px"}} className="d-inline-block" onMouseOver={this.onMouseEnter} onMouseLeave={this.onMouseLeave} isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle >
          {this.props.headerName}
        </DropdownToggle>
        {
            optionList ? 
                <DropdownMenu style={{fontSize:"12px", backgroundColor:"white"}}>
                    {optionList}
                </DropdownMenu>
                :
                null
        }
        
      </Dropdown>
    );
  }
}