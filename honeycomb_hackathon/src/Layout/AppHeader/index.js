import React, {Fragment} from 'react';
import avatar1 from '../../assets/betting-logo/bettingLogo.png';
import DropdownNav from "./DropdownNavbar/DropdownHoverNavbar";


//Background Image
import bg1 from "../../assets/insurance/homepage_bg.jpeg";

class Header extends React.Component {
    render() {
        return (
            <Fragment>

                <nav style={{width : "100%", height:"50px", backgroundColor:"green"}}>
                
                    <a className="navbar-brand" href="#/hackathon/home"  style={{ color:"white", marginLeft : "50px", marginRight : "200px"}}>
                        Secure Crops Insurance
                    </a>
                    
                    <DropdownNav headerName={"FARMER'S ROOM"} options={["My Insurance Schemes"]} 
                        hrefs={["#/hackathon/my-scheme-list"]}/>
                    
                    <DropdownNav headerName={"INSURANCE COMPANY"} options={["Registration", "Profile"]} 
                        hrefs={["#/hackathon/company-registration", "#/hackathon/company-profile"]}/>
                    
                </nav>
               
            </Fragment>
        );
    }
}

export default Header;